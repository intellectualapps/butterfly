//
//  ViewController.swift
//  Zakatify
//
//  Created by Nguyen Van Dung on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit

class LoginViewController: UIViewController, StartView {
    var presenter: StartViewPresenter!

    @IBOutlet weak var facebookBtn: Button!
    @IBOutlet weak var twitterBtn: Button!
    @IBOutlet weak var emailBtn: Button!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.facebookBtn?.titleLabel?.adjustsFontSizeToFitWidth = true
        self.facebookBtn?.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        self.twitterBtn?.titleLabel?.adjustsFontSizeToFitWidth = true
        self.twitterBtn?.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        self.emailBtn?.titleLabel?.adjustsFontSizeToFitWidth = true
        self.emailBtn?.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        // Do any additional setup after loading the view, typically from a nib.
        presenter = StartPresenter(self)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickFacebook() {
        presenter.loginFacebook()
    }
    
    @IBAction func onClickTwitter() {
        presenter.loginTwitter()
    }
    
    // TODO : assign a.Thang
    func gotoLoginByTwitter(user:UserInfo) {
        performSegue(withIdentifier: "gotoCreateUser", sender: user)

    }
    // TODO : assign a.Thang
    func gotoLoginByFacebook(user:UserInfo) {
        performSegue(withIdentifier: "gotoCreateUser", sender: user)
    }
    // TODO : assign Minh
    
    func gotoHome() {
        AppDelegate.shareInstance().gotoHomeViewController()
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier ?? "" {
        case "gotoCreateUser":
            if let navagation = segue.destination as? UINavigationController {
                if let createUserVC = navagation.viewControllers.first as? CreateUserViewController, let user = sender as? UserInfo {
                    createUserVC.user = user
                }
            }
            
        default:
            return
        }
    }
}


protocol StartView: CommonView {
    func gotoLoginByFacebook(user:UserInfo)
    func gotoLoginByTwitter(user:UserInfo)
    func gotoHome()
}

protocol StartViewPresenter: class {
    init(_ view: StartView)
    func loginFacebook()
    func getFacebookEmail()
    func loginTwitter()
    func getTwitterEmail()
}

class StartPresenter: StartViewPresenter {
    unowned let view: StartView
    let service: UserServices = UserServicesCenter()
    required init(_ view: StartView) {
        self.view = view
    }
    
    func loginFacebook() {
        if let _ = FBSDKAccessToken.current() {
            getFacebookEmail()
            return
        }
        let manager = FBSDKLoginManager()
        manager.logIn(withReadPermissions: ["public_profile","email"], from: view as! UIViewController) { [weak self] (result, error) in
            guard let strongSelf = self else {
                return
            }
            if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            } else if let result = result {
                if result.isCancelled {
                    
                } else {
                    strongSelf.getFacebookEmail()
                }
            }
        }
    }
    
    func getFacebookEmail() {
        guard let _ = FBSDKAccessToken.current() else {
            return
        }
        let params = ["fields":"id,name,email"]
        view.showLoading()
        FBSDKGraphRequest(graphPath: "me", parameters: params).start { [weak self] (connection, result, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            } else if let result = result as? [String:Any] {
                if let email = result["email"] as? String {
                    strongSelf.loginFacebook(email: email)
                }
            }
        }
    }
    
    func loginFacebook(email:String) {
        view.showLoading()
        service.loginFaceBook(email) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let user):
                UserManager.shared.currentUser = user
                UserManager.shared.authenToken = user.authToken
                if user.username.isEmpty {
                    strongSelf.view.gotoLoginByFacebook(user: user)
                } else {
                    strongSelf.view.gotoHome()
                }
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    func loginTwitter() {
        let client = TWTRAPIClient.withCurrentUser()
        if let _ = client.userID {
            getTwitterEmail()
            return
        }
        Twitter.sharedInstance().logIn(completion: {[weak self] (session, error) in
            guard let strongSelf = self else {
                return
            }
            if (session != nil) {
                strongSelf.getTwitterEmail()
            } else if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            }
        })
    }
    
    func getTwitterEmail() {
        let client = TWTRAPIClient.withCurrentUser()
        view.showLoading()
        client.requestEmail { [weak self] email, error in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()

            if let email = email {
                strongSelf.loginTwitter(email: email)
            } else if let error = error {
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    func loginTwitter(email:String) {
        view.showLoading()
        service.loginTwitter(email) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let user):
                UserManager.shared.currentUser = user
                UserManager.shared.authenToken = user.authToken
                if user.username.isEmpty {
                    strongSelf.view.gotoLoginByTwitter(user: user)
                } else {
                    strongSelf.view.gotoHome()
                }
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    
}

