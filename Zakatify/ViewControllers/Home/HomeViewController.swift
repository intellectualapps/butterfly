//
//  HomeViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController, TableView {
    var presenter: CharityListViewPresenter!
    
    var refPresenter: TableViewPresenter? {
        guard let presenter = self.presenter else {
            return nil
        }
        return presenter
    }
    
    lazy var searchNavigationController: UINavigationController = {
        let navigation = UIStoryboard.home().instantiateViewController(withIdentifier: "searchNavition") as! UINavigationController
        return navigation
    }()
    
    lazy var searchResultsController: SearchResultViewController = { [unowned self] in
        let vc = self.searchNavigationController.viewControllers.first as! SearchResultViewController
        return vc
    }()
    
    lazy var searchController: UISearchController = { [unowned self] in
        let searchController = UISearchController(searchResultsController: self.searchNavigationController)
        searchController.searchResultsUpdater = self.searchResultsController
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        let searchBar = searchController.searchBar
        searchBar.tintColor = UIColor.navigationBartinColor

        searchBar.scopeButtonTitles = [SearchResultViewController.ScopeType.charity.rawValue,
                                       SearchResultViewController.ScopeType.zakatifier.rawValue]
        searchBar.scopeBarBackgroundImage = UIImage(color: UIColor.white)
        searchBar.setScopeBarButtonBackgroundImage(#imageLiteral(resourceName: "scope"), for: UIControlState.selected)
        searchBar.setScopeBarButtonBackgroundImage(#imageLiteral(resourceName: "scope"), for: UIControlState.highlighted)
        searchBar.setScopeBarButtonBackgroundImage(#imageLiteral(resourceName: "scope-unselect"), for: UIControlState.normal)
        searchBar.setScopeBarButtonTitleTextAttributes([NSForegroundColorAttributeName:UIColor.darkGray], for: UIControlState.highlighted)
        searchBar.setScopeBarButtonTitleTextAttributes([NSForegroundColorAttributeName:UIColor.darkGray], for: UIControlState.selected)
        searchBar.setScopeBarButtonTitleTextAttributes([NSForegroundColorAttributeName:UIColor.lightGray], for: UIControlState.normal)
        
        searchBar.setScopeBarButtonDividerImage(UIImage(color: UIColor.white, size: CGSize(width: 5, height: 44)), forLeftSegmentState: .normal, rightSegmentState: .highlighted)
        searchBar.setScopeBarButtonDividerImage(UIImage(color: UIColor.white, size: CGSize(width: 5, height: 44)), forLeftSegmentState: .highlighted, rightSegmentState: .normal)
        
        if let scope = searchBar.traverseSubviewForViewOfKind(kind: UISegmentedControl.self) {
            scope.frame.origin.y = 0
            scope.frame.size.height = 44
        }
        return searchController
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

        tableView.tableHeaderView = searchController.searchBar
        tableView.register(UINib(nibName: "CharityDetailTableViewCell", bundle: nil), forCellReuseIdentifier: CharityDetailTableViewCell.identifier)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlChange), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        presenter = CharityListPresenter(view: self)
        presenter.refresh()
    }
    
    func refreshControlChange() {
        presenter?.refresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func logout(_ sender: Any) {
        UserManager.shared.logout()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfRowsInSection(section: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CharityDetailTableViewCell.identifier, for: indexPath) as? CharityDetailTableViewCell else {
            return UITableViewCell()
        }
        if let model = presenter.charity(index: indexPath.row) {
            let cellPresenter = CharityDetailPresenter(view: cell, model: model)
            cell.presenter = cellPresenter
            cell.clickAddBlock = { [unowned self] in
                self.presenter?.add(charity: model)
            }
            
            cell.clickRemoveBlock = { [unowned self] in
                self.presenter?.remove(charity: model)
            }
        } else {
            cell.presenter = nil
            cell.clickAddBlock = nil
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = tableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == lastElement - 1 {
            presenter?.loadmore()
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = UIStoryboard.charity().instantiateViewController(withIdentifier: "CharityDetailsViewController") as? CharityDetailsViewController else {
            return
        }
        if let model = presenter.charity(index: indexPath.row) {
            vc.charity = model
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


