//
//  CharityDetailTableViewCell.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import AlamofireImage

class CharityDetailTableViewCell: UITableViewCell {
    static let identifier: String = "CharityDetailTableViewCell"
    @IBOutlet weak var lb_newfeed: UILabel!
    @IBOutlet weak var iv_logo: UIImageView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_description: UILabel!
    @IBOutlet weak var lb_tag: UILabel!
    @IBOutlet weak var v_donorAvatars: UIView!
    @IBOutlet weak var lb_donors: UILabel!
    
    lazy var bt_add: UIButton  = {
        let bt = UIButton(type: UIButtonType.custom)
        bt.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        bt.setImage(#imageLiteral(resourceName: "ic-add"), for: UIControlState.normal)
        bt.addTarget(self, action: #selector(self.addPortfolio), for: UIControlEvents.touchUpInside)
        return bt
    }()
    lazy var bt_remove: UIButton = {
        let bt = UIButton(type: UIButtonType.custom)
        bt.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        bt.setImage(#imageLiteral(resourceName: "ic-check"), for: UIControlState.normal)
        bt.addTarget(self, action: #selector(self.removePortfolio), for: UIControlEvents.touchUpInside)
        return bt
    }()
    
    var presenter: CharityDetailViewPresenter? {
        didSet {
            dataChanged()
        }
    }
    
    var clickAddBlock:(()->Void)?
    
    var clickRemoveBlock:(()->Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        iv_logo.backgroundColor = UIColor.groupTableViewBackground
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension CharityDetailTableViewCell: CharityDetailView {
    
    func userDidAddToPorfolio() {
        
    }
    func userDidDonate() {
        
    }
    func userDidWriteReview() {
        
    }
    func dataChanged() {
        guard let presenter = presenter else {
            return
        }
        lb_newfeed.attributedText = presenter.charityNewFeed
        lb_name.text = presenter.charityName
        lb_description.text = presenter.charityDesription
        if let url = URL(string: presenter.charityLogoUrl) {
            iv_logo.af_setImage(withURL: url)
        }
        let donorsString = "\(presenter.donateDescription) \(presenter.moneyDescription)"
        lb_donors.text = donorsString
        lb_tag.text = presenter.tagDescription
        var count = 0
        let width = v_donorAvatars.frame.size.height
        for iv in v_donorAvatars.subviews {
            iv.removeFromSuperview()
        }
        let rate = v_donorAvatars.frame.size.width / 5.0
        for url in presenter.donorsAvatarUrls {
            let x = CGFloat(count) * rate
            let iv = UIImageView(frame: CGRect(x: x, y: 0, width: width, height: width))
            iv.layer.masksToBounds = true
            iv.layer.cornerRadius = width/2.0
            iv.layer.borderWidth = 1.5
            iv.layer.borderColor = UIColor.white.cgColor
            iv.af_setImage(withURL: url)
            v_donorAvatars.addSubview(iv)
            v_donorAvatars.sendSubview(toBack: iv)
            count += 1
        }
        
        if presenter.userPortfolio {
            self.accessoryView = bt_remove
        } else {
            self.accessoryView = bt_add
        }
    }
    
    func addPortfolio() {
        self.clickAddBlock?()
    }
    
    func removePortfolio() {
        self.clickRemoveBlock?()
    }
}
