//
//  SearchResultViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/4/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
class SearchResultViewController: UITableViewController, TableView, ListZakatifiersView, UISearchResultsUpdating {
    
    var searchString:String?
    enum ScopeType: String {
        case charity = "CHARITIES"
        case zakatifier = "ZAKATIFIERS"
    }
    var type: ScopeType = .charity
    var charityPresenter: CharityListViewPresenter?
    var zakatifierPresenter: ListZakatifiersViewPresenter?
    
    var refPresenter: TableViewPresenter? {
        var prt: TableViewPresenter?
        switch type {
        case .zakatifier:
            prt = zakatifierPresenter
            break
        default:
            prt = charityPresenter
        }
        guard let presenter = prt else {
            return nil
        }
        return presenter
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem(z
        tableView.register(UINib(nibName: "CharityDetailTableViewCell", bundle: nil), forCellReuseIdentifier: CharityDetailTableViewCell.identifier)
        tableView.register(UINib(nibName: "ZakatifierTableViewCell", bundle: nil), forCellReuseIdentifier: ZakatifierTableViewCell.identifier)

        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlChange), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        charityPresenter = CharityListPresenter(view: self)
        zakatifierPresenter = ListZakatifiersPresenter(view: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshControlChange() {
        refPresenter?.refresh(search: searchString)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        searchString = searchController.searchBar.text
        if let scopeTitle = searchController.searchBar.scopeButtonTitles?[searchController.searchBar.selectedScopeButtonIndex] {
            if let scope = ScopeType(rawValue: scopeTitle) {
                self.type = scope
            }
        }
        refreshControlChange()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        guard let presenter = refPresenter else {
            return 0
        }
        return presenter.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard let presenter = refPresenter else {
            return 0
        }
        return presenter.numberOfRowsInSection(section: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if type == .zakatifier {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ZakatifierTableViewCell.identifier, for: indexPath) as? ZakatifierTableViewCell else {
                return UITableViewCell()
            }
            
            // Configure the cell...
            if let zakatifier = zakatifierPresenter?.zakatifer(at: indexPath) {
                cell.zakatifier = zakatifier
            }
            cell.clickAddBlock = { [weak self] zakatifier in
                guard let zakatifier = zakatifier else { return }
                self?.zakatifierPresenter?.addZakatifier(zakatifier)
            }
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CharityDetailTableViewCell.identifier, for: indexPath) as? CharityDetailTableViewCell else {
                return UITableViewCell()
            }
            if let model = charityPresenter?.charity(index: indexPath.row) {
                let cellPresenter = CharityDetailPresenter(view: cell, model: model)
                cell.presenter = cellPresenter
                cell.clickAddBlock = { [unowned self] in
                    self.charityPresenter?.add(charity: model)
                }
            } else {
                cell.presenter = nil
                cell.clickAddBlock = nil
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = tableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == lastElement - 1 {
            refPresenter?.loadmore()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if type == .zakatifier {
            if let model = zakatifierPresenter?.zakatifer(at: indexPath) {
                
            }
        } else {
            if let model = charityPresenter?.charity(index: indexPath.row) {
                guard let vc = UIStoryboard.charity().instantiateViewController(withIdentifier: "CharityDetailsViewController") as? CharityDetailsViewController else {
                    return
                }
                vc.charity = model
                let navigation = NavigationController(rootViewController: vc)
                self.present(navigation, animated: true, completion: nil)
            }
        }
    }
}
