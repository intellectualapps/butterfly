//
//  File.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import UserNotifications
import MBProgressHUD

public protocol CommonView: class {
    func showLoading()
    func hideLoading()
    func showAlert(_ message:String)
}

extension UIViewController: CommonView {
    public func showLoading() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    public func hideLoading() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    public func showAlert(_ message:String) {
//        let title = "Message from Zakatify"
//        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        let ok = UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil)
//        alert.addAction(ok)
//        self.present(alert, animated: true, completion: nil)
        
        let content = UNMutableNotificationContent()
        content.body = message
        
        //Set the trigger of the notification -- here a timer.
        let trigger = UNTimeIntervalNotificationTrigger(
            timeInterval: 0.01,
            repeats: false)
        
        //Set the request for the notification from the above
        let request = UNNotificationRequest(
            identifier: "Local.Alert",
            content: content,
            trigger: trigger
        )
        
        //Add the notification to the currnet notification center
        UNUserNotificationCenter.current().add(
            request, withCompletionHandler: nil)
    }
}

//extension UITableViewController {
//    
//    public override func showLoading() {
//        guard let superView = self.view.superview else {
//            MBProgressHUD.showAdded(to: self.view, animated: true)
//            return
//        }
//        MBProgressHUD.showAdded(to: superView, animated: true)
//    }
//    
//    public override func hideLoading() {
//        guard let superView = self.view.superview else {
//            MBProgressHUD.hide(for: self.view, animated: true)
//            return
//        }
//        MBProgressHUD.hide(for: superView, animated: true)
//    }
//
//}

extension UIView: CommonView {
    public func showLoading() {
        MBProgressHUD.showAdded(to: self, animated: true)
    }
    
    public func hideLoading() {
        MBProgressHUD.hide(for: self, animated: true)
    }
    public func showAlert(_ message:String) {
        //        let title = "Message from Zakatify"
        //        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        //        let ok = UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil)
        //        alert.addAction(ok)
        //        self.present(alert, animated: true, completion: nil)
        
        let content = UNMutableNotificationContent()
        content.body = message
        
        //Set the trigger of the notification -- here a timer.
        let trigger = UNTimeIntervalNotificationTrigger(
            timeInterval: 0.01,
            repeats: false)
        
        //Set the request for the notification from the above
        let request = UNNotificationRequest(
            identifier: "Local.Alert",
            content: content,
            trigger: trigger
        )
        
        //Add the notification to the currnet notification center
        UNUserNotificationCenter.current().add(
            request, withCompletionHandler: nil)
    }
}
