//
//  DataServices.swift
//  Layout
//
//  Created by Trương Thắng on 5/27/17.
//  Copyright © 2017 Trương Thắng. All rights reserved.
//

import Foundation

class DataServices {
    static let shared: DataServices = DataServices()
    
    private var _usingGuides : [UsingGuide]?
    var usingGuides : [UsingGuide] {
        get{
            if _usingGuides == nil {
                updateusingGuide()
            }
            return _usingGuides ?? []
        }
        set{
            _usingGuides = newValue
        }
    }
    
    func updateusingGuide() {
        _usingGuides = []
        let model = UsingGuide(titleLabel: "ADD FAVORITE CHARITIES", imageIntro: #imageLiteral(resourceName: "Group"), descriptionLabel: "Lorem ipsum dolor sit amet,\nconsectetuer adipiscing elit,\nsed diam nonummy nibh")
        
        let model2 = UsingGuide(titleLabel: "ADD PAYMENT OPTIONS", imageIntro: #imageLiteral(resourceName: "onboarding-2"), descriptionLabel: "Lorem ipsum dolor sit amet,\nconsectetuer adipiscing elit,\nsed diam nonummy nibh")
        
        let model3 = UsingGuide(titleLabel: "ADD ZAKATIFY PREFERENTCES", imageIntro: #imageLiteral(resourceName: "onboarding-1"), descriptionLabel: "Lorem ipsum dolor sit amet,\nconsectetuer adipiscing elit,\nsed diam nonummy nibh")
        
        let model4 = UsingGuide(titleLabel: "SHARE WITH ORTHER", imageIntro: #imageLiteral(resourceName: "onboarding-5"), descriptionLabel: "Lorem ipsum dolor sit amet,\nconsectetuer adipiscing elit,\nsed diam nonummy nibh")
        
        _usingGuides?.append(model)
        _usingGuides?.append(model2)
        _usingGuides?.append(model3)
        _usingGuides?.append(model4)
    }
    
}
