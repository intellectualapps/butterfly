//
//  ForgotEmailViewController.swift
//  Zakatify
//
//  Created by Thang Truong on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class ForgotEmailViewController: UIViewController, ForgetPasswordView {
    @IBOutlet weak var tf_email: UITextField!
    var presenter:ForgetPasswordPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        presenter = ForgetPasswordPresenter(view: self)
    }
    @IBAction func onClickResetPassword(_ sender: Any) {
        guard let email = tf_email.text else {
            return
        }
        presenter.requestResetPassword(email: email)
    }

    func didRequestResetPassword() {
        self.showAlert("Sent request")
    }
    
    @IBAction func onClickBackButton() {
        self.dismiss(animated: true, completion: nil)
    }
}
