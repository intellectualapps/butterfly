//
//  PreferencesViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import RxSwift

class PreferencesViewController: UITableViewController, PreferenceView {
    var disposeBag:DisposeBag = DisposeBag()
    @IBOutlet weak var tf_money: NumberTextFeild!
    @IBOutlet weak var iv_underline: UIImageView!
    @IBOutlet weak var iv_underline_leading: NSLayoutConstraint!
    @IBOutlet weak var bt_save: Button!
    
    enum Mode {
        case new
        case edit
    }
    
    var mode: Mode = .new {
        didSet {
            switch mode {
            case .new:
                bt_save?.setTitle("Next: Add payment options", for: UIControlState.normal)
            case .edit:
                bt_save?.setTitle("Save changes", for: UIControlState.normal)
            }
        }
    }
    
    var z: NSNumber = NSNumber(value: 0) {
        didSet {
            guard let tf_money = tf_money else {
                return
            }
            tf_money.text = "\(z.floatValue)"
        }
    }
    
    var frequency: Zakat.frequency = .auto {
        didSet {
            guard let iv_underline_leading = iv_underline_leading , let iv_underline = iv_underline else {
                return
            }
            switch frequency {
            case .auto:
                iv_underline_leading.constant = 15
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3) {
                        self.view.layoutIfNeeded()
                    }
                }
            case .manual:
                iv_underline_leading.constant = 15 + 8 + iv_underline.frame.size.width
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3) {
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    
    var presenter: PreferencePresenter?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.addBackButtonDefault()
        let mode = self.mode
        self.mode = mode
        
        presenter = PreferencePresenter(view: self)
        tf_money.text = "0"
        tf_money.rx.controlEvent(UIControlEvents.editingChanged).bind { [unowned self] () in
            if let number = self.tf_money.number {
                self.presenter?.zakat.amount = number.floatValue
            }
        }.addDisposableTo(disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func next(_ sender: Any) {
        presenter?.addZakatGoal(amount: z.floatValue)
    }
    @IBAction func selectAuto(_ sender: Any) {
        self.frequency = .auto
        presenter?.frequency = .auto
    }
    @IBAction func selectManual(_ sender: Any) {
        self.frequency = .manual
        presenter?.frequency = .manual
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "calculator" {
            if let navigation = segue.destination as? UINavigationController {
                if let vc = navigation.viewControllers.first as? CalculateGoalViewController {
                    vc.zakat = presenter?.zakat.copy() as? Zakat
                    vc.delegate = self
                }
            }
        }
    }
    
    func fillData() {
        guard let presenter = presenter else {
            return
        }
        frequency = presenter.frequency
        z = presenter.z
    }
    
    func saveSuccess() {
        switch mode {
        case .new:
            self.performSegue(withIdentifier: "payment", sender: nil)
        case .edit:
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let footer = tableView.tableFooterView , let window = AppDelegate.shareInstance().window else {
            return
        }
        let windowFrame = window.frame
        let frameInWindow = tableView.convert(footer.frame, to: window)
        var height = windowFrame.size.height - frameInWindow.origin.y
        if height <= 60 {
            height = 60
        }
        footer.frame.size.height = height
    }

}

extension PreferencesViewController: CalculateGoalViewControllerDelegate {
    func didCalculateZ(zakat: Zakat) {
        presenter?.zakat = zakat
    }
}
