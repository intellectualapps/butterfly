//
//  PaymentTableViewCell.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

protocol PaymentTableViewCellDelegate: class {
    func clickMakePrimary(payment:Payment?, cell:PaymentTableViewCell)
    func clickDelete(payment:Payment?, cell:PaymentTableViewCell)
}

class PaymentTableViewCell: UITableViewCell {
    static var identifier = "PaymentTableViewCell"
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_primary: UILabel!
    @IBOutlet weak var iv_content: UIView!
    @IBOutlet weak var iv_content_leading: NSLayoutConstraint!
    @IBOutlet weak var iv_content_trailing: NSLayoutConstraint!
    
    @IBOutlet weak var lb_swipe: UILabel!
    @IBOutlet weak var iv_check: UIImageView!
    @IBOutlet weak var bt_MakePrimary: Button!
    enum Mode {
        case swipe
        case select(status:Bool)
    }
    
    var cellMode: Mode = .swipe {
        didSet {
            switch cellMode {
            case .select(let status):
                iv_check.isHidden = !status
                lb_swipe.isHidden = true
                break
            default:
                iv_check.isHidden = true
                lb_swipe.isHidden = false
                break
            }
        }
    }
    
    weak var delegate: PaymentTableViewCellDelegate?
    var payment: Payment? {
        didSet {
            guard let payment = payment else {
                return
            }
            
            guard let lb_name = lb_name, let lb_primary = lb_primary else {
                return
            }
            
            if payment.isPrimary == 1 {
                lb_primary.isHidden = false
                lb_primary.text = "Primary"
                bt_MakePrimary.isHidden = true
            } else {
                lb_primary.isHidden = true
                lb_primary.text = "Make Primary"
                bt_MakePrimary.isHidden = false
            }
            lb_name.text = payment.email
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        iv_content.layer.borderWidth = 1
        iv_content.layer.borderColor = UIColor.lightGray.cgColor
        iv_content.layer.cornerRadius = 5
        iv_content.layer.masksToBounds = true
        iv_content.backgroundColor = UIColor.white
        iv_content.isUserInteractionEnabled = true
        let swipeleft = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeLeft))
        swipeleft.direction = .left
        self.iv_content.addGestureRecognizer(swipeleft)
        
        let swiperight = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRight))
        swiperight.direction = .right
        self.iv_content.addGestureRecognizer(swiperight)
    }
    func swipeLeft() {
        switch self.cellMode {
        case .swipe:
            break
        default:
            return
        }
        
        if iv_content_leading.constant != 0 {
            return
        }
        self.iv_content_leading.constant -= 180
        self.iv_content_trailing.constant += 180
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3) {
                self.layoutIfNeeded()
            }
        }
    }
    
    func swipeRight() {
        switch self.cellMode {
        case .swipe:
            break
        default:
            return
        }
        self.iv_content_leading.constant = 0.0
        self.iv_content_trailing.constant = 0.0
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3) {
                self.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func clickMakePrimary(_ sender: Any) {
        makePrimary()
    }
    
    func makePrimary() {
        print("Make primary")
        swipeRight()
        delegate?.clickMakePrimary(payment: payment, cell: self)
    }
    @IBAction func clickDeletePayment(_ sender: Any) {
        deletePayment()
    }
    
    func deletePayment() {
        print("Delete")
        swipeRight()
        delegate?.clickDelete(payment: payment, cell: self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
