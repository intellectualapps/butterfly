//
//  NumberInputTableViewCell.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import RxSwift
import AMPopTip

protocol NumberInputTableViewCellDelete: class {
    func selectToolTipButton(cell:NumberInputTableViewCell)
}

class NumberInputTableViewCell: UITableViewCell {
    static var identifier: String = "NumberInputTableViewCell"
    var disposeBag = DisposeBag()
    var bt: UIButton!
    
    weak var delegate:NumberInputTableViewCellDelete?
    
    let popTip = PopTip()
    var direction = PopTipDirection.left
    var topRightDirection = PopTipDirection.down
    
    @IBOutlet weak var tf_input: NumberTextFeild! {
        didSet {
            guard let tf_input = tf_input else {
                return
            }
            tf_input.rx.controlEvent(UIControlEvents.editingChanged).bind { [unowned self] (event) in
                guard let data = self.data else {
                    return
                }
                guard let zakat = self.zakat else {
                    return
                }
                guard let number = tf_input.number else {
                    return
                }
                switch data {
                case .cash:
                    zakat.ce = number.floatValue
                case .gold:
                    zakat.gs = number.floatValue
                case .real:
                    zakat.re = number.floatValue
                case .inves:
                    zakat.i = number.floatValue
                case .personal:
                    zakat.pu = number.floatValue
                case .liabiliti:
                    zakat.l = number.floatValue
                }
            }.addDisposableTo(disposeBag)
        }
    }
    
    var data: CalculateGoalViewController.CellData? {
        didSet {
            guard let data = data else {
                return
            }
            tf_input.placeholder = data.placesHoldle
            guard let zakat = zakat else {
                return
            }
            var value: Float
            switch data {
            case .cash:
                value = zakat.ce
            case .gold:
                value = zakat.gs
            case .real:
                value = zakat.re
            case .inves:
                value = zakat.i
            case .personal:
                value = zakat.pu
            case .liabiliti:
                value = zakat.l
            }
            let number = NSNumber(value: value)
            tf_input.text = "\(number.intValue)"
        }
    }
    
    var zakat: Zakat?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bt = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        bt.setImage(#imageLiteral(resourceName: "ic-question"), for: UIControlState.normal)
        bt.addTarget(self, action: #selector(self.selectQuestion), for: UIControlEvents.touchUpInside)
        tf_input.rightView = bt
        tf_input.rightViewMode = .always
        popTip.cornerRadius = 10
        popTip.arrowSize = CGSize(width: 15, height: 10)
        popTip.shouldShowMask = true
        popTip.bubbleColor = UIColor.white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func selectQuestion() {
        delegate?.selectToolTipButton(cell: self)
        let customView = UIView.loadFromNibNamed(nibNamed: "ToolTipView")!
        customView.frame = CGRect(x: 0, y: 0, width: 226, height: 278)
        let frame = bt.superview?.convert(bt.frame, to: AppDelegate.shareInstance().window)
        popTip.show(customView: customView, direction: .left, in: AppDelegate.shareInstance().window!, from: frame!)

    }
}
