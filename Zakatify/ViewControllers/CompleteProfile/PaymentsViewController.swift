//
//  PaymentsViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class PaymentsViewController: UIViewController, CreditCardView, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var iv_payment: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bt_next: Button!
    @IBOutlet weak var bt_bottomSpace_addCrediCard: NSLayoutConstraint!
    
    enum Mode {
        case new
        case edit
    }
    
    var mode: Mode = .new {
        didSet {
            switch mode {
            case .new:
                bt_next?.isHidden = false
                bt_bottomSpace_addCrediCard?.constant = 90
            case .edit:
                bt_next?.isHidden = true
                bt_bottomSpace_addCrediCard?.constant = 25
            }
            self.view.layoutIfNeeded()
        }
    }
    
    var refTableView: UITableView? {
        return tableView
    }
    
    var presenter: CreditCardViewPresenter?
    var refPresenter: TableViewPresenter? {
        return presenter
    }
    
    var payPalConfiguration = PayPalConfiguration()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        payPalConfiguration.merchantName = "Zakatify"
        payPalConfiguration.merchantPrivacyPolicyURL = URL(string: "https://www.google.com.vn")
        payPalConfiguration.merchantUserAgreementURL = URL(string: "https://www.google.com.vn")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addBackButtonDefault()
        let mode = self.mode
        self.mode = mode
        presenter = CreditCardPresenter(view: self)
        
        self.tableView.estimatedRowHeight = 70
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(UINib(nibName: "PaymentTableViewCell", bundle: nil) , forCellReuseIdentifier: PaymentTableViewCell.identifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        #if DEBUG
            PayPalMobile.preconnect(withEnvironment: PayPalEnvironmentSandbox)
        #else
            PayPalMobile.preconnect(withEnvironment: PayPalEnvironmentProduction)
        #endif
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addPayment(_ sender: Any) {
        guard let presenter = presenter else {
            return
        }
        getPaypalProfile()
//        if let fpvc = PayPalFuturePaymentViewController(configuration: payPalConfiguration, delegate: self) {
//            self.present(fpvc, animated: true, completion: nil)
//        }
    }
    
    @IBAction func next(_ sender: Any) {
        guard let presenter = presenter else {
            return
        }
        if presenter.numberOfRowsInSection(section: 0) > 0 {
            self.performSegue(withIdentifier: "tag", sender: nil)
        }
    }
    
    func loadingMore() {
        
    }
    
    func loadedMore() {
        
    }
    
    func insert(indexPaths: [IndexPath]) {
        
    }
    
    func refreshing() {
        showLoading()
    }
    
    func refreshed() {
        hideLoading()
        guard let presenter = presenter else {
            iv_payment.isHidden = false
            return
        }
        if presenter.numberOfRowsInSection(section: 0) == 0 {
            iv_payment.isHidden = false
        } else {
            iv_payment.isHidden = true
        }
        tableView.reloadData()
    }
    
    func saveSuccess() {
        guard let presenter = presenter else {
            iv_payment.isHidden = false
            return
        }
        if presenter.numberOfRowsInSection(section: 0) == 0 {
            iv_payment.isHidden = false
        } else {
            iv_payment.isHidden = true
        }
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let presenter = presenter else {
            return 0
        }
        return presenter.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentTableViewCell.identifier, for: indexPath) as? PaymentTableViewCell else {
            return UITableViewCell()
        }
        
        let payment = presenter?.paymentAt(indexPath: indexPath)
        cell.payment = payment
        cell.delegate = self
        return cell
    }

}

extension PaymentsViewController: PaymentTableViewCellDelegate {
    func clickMakePrimary(payment: Payment?, cell: PaymentTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        presenter?.makePrimary(index: indexPath)
    }
    
    func clickDelete(payment: Payment?, cell: PaymentTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        presenter?.delete(index: indexPath)
    }
}
extension PaymentsViewController: PayPalFuturePaymentDelegate {
    func getPaypalForFuture() {
        if let fpvc = PayPalFuturePaymentViewController(configuration: payPalConfiguration, delegate: self) {
            self.present(fpvc, animated: true, completion: nil)
        }
    }
    
    func payPalFuturePaymentDidCancel(_ futurePaymentViewController: PayPalFuturePaymentViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func payPalFuturePaymentViewController(_ futurePaymentViewController: PayPalFuturePaymentViewController, didAuthorizeFuturePayment futurePaymentAuthorization: [AnyHashable : Any]) {
        self.dismiss(animated: true, completion: nil)
        if let response = futurePaymentAuthorization["response"] as? [String:String] {
            if let code = response["code"] {
                let payment = Payment()
                payment.token = code
                payment.lastFourDigit = 1234
                self.presenter?.add(payment: payment)
            }
        }
    }

}

extension PaymentsViewController: PayPalProfileSharingDelegate {
    func getPaypalProfile() {
        let scope = Set([kPayPalOAuth2ScopeFuturePayments,kPayPalOAuth2ScopeEmail])
        if let vc = PayPalProfileSharingViewController(scopeValues: scope, configuration: payPalConfiguration, delegate: self) {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func userDidCancel(_ profileSharingViewController: PayPalProfileSharingViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func payPalProfileSharingViewController(_ profileSharingViewController: PayPalProfileSharingViewController, userDidLogInWithAuthorization profileSharingAuthorization: [AnyHashable : Any]) {
        if let response = profileSharingAuthorization["response"] as? [String:String] {
            if let code = response["code"] {
                let payment = Payment()
                payment.token = code
                payment.lastFourDigit = 1234
                self.presenter?.add(payment: payment)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
}

