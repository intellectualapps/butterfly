//
//  CalculateGoalViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
import PagingMenuController

protocol CalculateGoalViewControllerDelegate: class {
    func didCalculateZ(zakat:Zakat)
}

class CalculateGoalViewController: UIViewController {
    
    enum CellData {
        case cash
        case gold
        case real
        case inves
        case personal
        case liabiliti
        var placesHoldle: String {
            switch self {
            case .cash:
                return "Cash and equivalents"
            case .gold:
                return "Gold and silver"
            case .real:
                return "Real estate"
            case .inves:
                return "Investments"
            case .personal:
                return "Personal use"
            case .liabiliti:
                return "Liabilities"
            }
        }
        var key: String {
            switch self {
            case .cash:
                return "ce"
            case .gold:
                return "gs"
            case .real:
                return "re"
            case .inves:
                return "i"
            case .personal:
                return "pu"
            case .liabiliti:
                return "l"
            }
        }
        
        var description: String {
            return ""
        }
    }
    @IBOutlet weak var tf_goal: UITextField!
    @IBOutlet weak var uv_container: UIView!
    weak var delegate: CalculateGoalViewControllerDelegate?
    var zakat: Zakat?
    var n: NSNumber?
    var nw: NSNumber = NSNumber(value: 0)
    var z: NSNumber = NSNumber(value: 0) {
        didSet {
            guard let tf_goal = tf_goal else {
                return
            }
            tf_goal.text = "\(z.floatValue)"
        }
    }
    
    private let viewController1 = WealthTableViewController(style: UITableViewStyle.plain)
    private let viewController2 = DeductionsTableViewController(style: UITableViewStyle.plain)
    
    private struct PagingMenuOptions: PagingMenuControllerCustomizable {
        
        fileprivate var componentType: ComponentType {
            return .all(menuOptions: MenuOptions(), pagingControllers: pagingControllers)
        }
        
        var pagingControllers: [UIViewController]
        
        fileprivate struct MenuOptions: MenuViewCustomizable {
            var displayMode: MenuDisplayMode {
                return .segmentedControl
            }
            var itemsOptions: [MenuItemViewCustomizable] {
                return [MenuItem1(), MenuItem2()]
            }
            var focusMode: MenuFocusMode {
                return .underline(height: 3, color: UIColor.navigationBartinColor, horizontalPadding: 15, verticalPadding: 0)
            }
        }
        
        fileprivate struct MenuItem1: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText(text: "WEALTH"))
            }
        }
        fileprivate struct MenuItem2: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText(text: "DEDUCTIONS"))
            }
        }
        
        init(controllers:[UIViewController]) {
            pagingControllers = controllers
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        addLeftCloseButton()
        viewController1.zakat = zakat
        viewController2.zakat = zakat
        let options = PagingMenuOptions(controllers: [viewController1, viewController2])

        
        let pagingMenuController = PagingMenuController(options: options)
        
        pagingMenuController.view.frame = uv_container.bounds
        addChildViewController(pagingMenuController)
        uv_container.addSubview(pagingMenuController.view)
        pagingMenuController.didMove(toParentViewController: self)
        guard let zakat = zakat else {
            return
        }
        self.z = NSNumber(value: zakat.amount)
        
    }
    
    func getDefaultValue() {
        self.showLoading()
        let service = GoldValueServicesCenter()
        service.getDefaultValue { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideLoading()
            switch result {
            case .success(let value):
                let number = NSNumber(value: value)
                strongSelf.n = number
                self?.calculate(self as Any)
            case .failure(let error):
                strongSelf.showAlert(error.localizedDescription)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func calculate(_ sender: Any) {
        guard let n = n else {
            getDefaultValue()
            return
        }
        self.view.endEditing(true)
        zakat?.calculate(n: n.floatValue)
        guard let zakat = zakat else {
            return
        }
        z = NSNumber(value:zakat.amount)
        delegate?.didCalculateZ(zakat: zakat)
    }
    @IBAction func clearZ(_ sender: Any) {
        z = NSNumber(value: 0)
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
