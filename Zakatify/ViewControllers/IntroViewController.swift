//
//  IntroViewController.swift
//  Layout
//
//  Created by Trương Thắng on 5/27/17.
//  Copyright © 2017 Trương Thắng. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!
    var timer: Timer?
    var collectionViewFlowLayout : CollectionViewFlowLayout!
    var pageViewController: UIPageViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.numberOfPages = DataServices.shared.usingGuides.count
        let width = UIScreen.main.bounds.size.width - 100
        let height = width * 1.3
        let itemSize = CGSize(width: width, height: height)
        collectionViewFlowLayout = CollectionViewFlowLayout.layoutConfigured(with: self.collectionView!, itemSize: itemSize, minimumLineSpacing: 0)
//        timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true);
        backButton.setTitle("", for: .normal)
        backButton.setImage(nil, for: .normal)
        backButton.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UICollectionViewDelegate, UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = DataServices.shared.usingGuides.count
        return DataServices.shared.usingGuides.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCollectionViewCell", for: indexPath) as! IntroCollectionViewCell
        let data = DataServices.shared.usingGuides
        cell.titleLabel.text = data[indexPath.row].titleLabel
        cell.imageIntro.image = data[indexPath.row].imageIntro
        cell.descriptionLable.text = data[indexPath.row].descriptionLabel
        return cell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        timer?.invalidate()
        timer = nil
    }
    
    @IBAction func scrollToPreviousCell() {
        pageControl.currentPage = pageControl.currentPage - 1
        scrollToIndex(index: pageControl.currentPage)
    }
    
    @IBAction func scrollToNextCell() {
        if pageControl.currentPage == pageControl.numberOfPages - 1 {
            if let navigation = UIStoryboard.completeProfile().instantiateInitialViewController() as? UINavigationController{
                self.navigationController?.setViewControllers(navigation.viewControllers, animated: true)
            }
        }
        pageControl.currentPage = pageControl.currentPage + 1
        scrollToIndex(index: pageControl.currentPage)
    }
    
    @IBAction func didChangePageControl() {
        scrollToIndex(index: pageControl.currentPage)
    }

    func scrollToIndex(index: Int) {
        configNextAndPreviousButton(index: index)
        let  indexPath = IndexPath(item: index, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func configNextAndPreviousButton(index: Int) {
        switch index {
        case 3:
            nextButton.setTitle("Get Started >", for: .normal)
            timer?.invalidate()
            timer = nil
            if nextButton?.title(for: .normal) == "Get Started >" {
                self.gotoNext()
            }
        case 1...2:
            nextButton.setTitle("Next >", for: .normal)
            backButton.setTitle("< Back", for: .normal)
            backButton.isEnabled = true
        case 0:
            backButton.setTitle("", for: .normal)
            backButton.setImage(nil, for: .normal)
            backButton.isEnabled = false
        default:
            break
        }
    }

    func gotoNext() {
        let storyboard = UIStoryboard(name: "CompleteProfile", bundle: nil)
        if let navi = storyboard.instantiateViewController(withIdentifier: "profileCompleteNavi") as? UINavigationController {
            self.present(navi, animated: true, completion: nil)
        }
    }
}

// MARK: - UIScrollViewDelegate

extension IntroViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPage = self.collectionView.contentOffset.x / self.collectionView.frame.size.width + 1
        self.pageControl.currentPage = Int(currentPage)
        configNextAndPreviousButton(index: pageControl.currentPage)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
}
