//
//  CharityHeaderTableViewCell.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/7/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
protocol CharityHeaderTableViewCellDelegate: class {
    func clickDonate()
    func clickAddPortfolio()
}

class CharityHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var uv_avatar_boder: UIView!
    @IBOutlet weak var iv_avatar: UIImageView!
    
    @IBOutlet weak var lb_charityName: UILabel!
    @IBOutlet weak var lb_rate: UILabel!
    
    @IBOutlet weak var uv_totalMoney_boder: UIView!
    @IBOutlet weak var tf_totoalMoney: NumberTextFeild!
    
    @IBOutlet weak var bt_add: Button!
    
    @IBOutlet weak var uv_donor: UIView!
    @IBOutlet weak var uv_donor_avatars: UIView!
    @IBOutlet weak var lb_donor_status: UILabel!
    
    @IBOutlet weak var lb_charityDescription: UILabel!
    
    @IBOutlet weak var lb_categories: UILabel!
    
    @IBOutlet weak var lb_ein: UILabel!
    @IBOutlet weak var bt_report: UIButton!
    
    @IBOutlet weak var lb_address: UILabel!
    @IBOutlet weak var lb_phoneNumber: UILabel!
    
    var presenter: CharityDetailViewPresenter? {
        didSet {
            dataChanged()
        }
    }
    
    weak var delegate: CharityHeaderTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uv_avatar_boder.addDashedBorder(color: UIColor.blueBorderColor, width: 3, space: 3)
        uv_totalMoney_boder.layer.cornerRadius = 5.0
        uv_totalMoney_boder.layer.masksToBounds = true
        let iv = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 38))
        iv.image = #imageLiteral(resourceName: "ic-dolar")
        tf_totoalMoney.leftView = iv
        tf_totoalMoney.leftViewMode = .always
        tf_totoalMoney.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func clickDonate(_ sender: Any) {
        delegate?.clickDonate()
    }

    @IBAction func clickAdd(_ sender: Any) {
        delegate?.clickAddPortfolio()
    }
    @IBAction func clickReport(_ sender: Any) {
        guard let presenter = presenter, let url = URL(string:presenter.charity.reportUrl) else {
            return
        }
        let _ = UIApplication.shared.openURL(url)
    }
    
}

extension CharityHeaderTableViewCell {
   
    func dataChanged() {
        guard let presenter = presenter else {
            return
        }
        lb_charityName.text = presenter.charityName
        tf_totoalMoney.text = "\(presenter.charity.totalMoney)"
        lb_charityDescription.text = presenter.charityDesription
        if let url = URL(string: presenter.charityLogoUrl) {
            iv_avatar.af_setImage(withURL: url)
        }
        lb_categories.text = presenter.tagDescription
        var rateText = ""
        for _ in 0..<presenter.charityRate {
            rateText += "⭑"
        }
        lb_rate.text = rateText
        
        if presenter.userPortfolio == true {
            bt_add.setTitle("Remove from Portfolio", for: UIControlState.normal)
        } else {
            bt_add.setTitle("Add to Portfolio", for: UIControlState.normal)
        }
        //
        var count = 0
        let width:CGFloat = 26
        for iv in uv_donor_avatars.subviews {
            iv.removeFromSuperview()
        }
        let rate:CGFloat = 26 * 0.7
        var uv_donor_avatars_Width:CGFloat = 0
        for url in presenter.donorsAvatarUrls {
            let x = CGFloat(count) * rate
            let iv = UIImageView(frame: CGRect(x: x, y: 0, width: width, height: width))
            iv.layer.masksToBounds = true
            iv.layer.cornerRadius = width/2.0
            iv.layer.borderWidth = 1.5
            iv.layer.borderColor = UIColor.white.cgColor
            iv.af_setImage(withURL: url)
            uv_donor_avatars.addSubview(iv)
            uv_donor_avatars.sendSubview(toBack: iv)
            uv_donor_avatars_Width = x + width
            count += 1
        }
        uv_donor_avatars.frame.size.width = uv_donor_avatars_Width
        
        lb_donor_status.text = presenter.donateDescription
        lb_donor_status.sizeToFit()
        let allWidth = uv_donor_avatars_Width + 5 + lb_donor_status.frame.size.width
        let uv_donor_avatars_x = (uv_donor.frame.size.width - allWidth)/2.0
        uv_donor_avatars.frame.origin.x = uv_donor_avatars_x
        let lb_donor_status_x = uv_donor_avatars.frame.origin.x + uv_donor_avatars.frame.size.width + 5
        lb_donor_status.frame.origin.x = lb_donor_status_x
        
        lb_ein.text = "EIN " + presenter.charity.ein
        lb_address.text = presenter.charity.address
        lb_phoneNumber.text = presenter.charity.phoneNumber
    }
    
}
