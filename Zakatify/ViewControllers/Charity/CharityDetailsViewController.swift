//
//  CharityDetailsViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/8/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class CharityDetailsViewController: UIViewController {
    
    var charityDetailTableView :CharityDetailsTableViewController?
    var presenter: CharityDetailViewPresenter? {
        didSet {
            charityDetailTableView?.presenter = presenter
        }
    }
    var charity: CharityDetail?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let _ = self.presentingViewController {
            self.addLeftCloseButton()
        } else {
            self.addBackButtonDefault()
        }

        if let charity = self.charity {
            presenter = CharityDetailPresenter(view: self, model: charity)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CharityDetailsTableViewController" {
            if let vc = segue.destination as? CharityDetailsTableViewController {
                self.charityDetailTableView = vc
                vc.presenter = presenter
            }
        }
    }
}

extension CharityDetailsViewController: CharityDetailView {
    func userDidAddToPorfolio() {
        self.charityDetailTableView?.tableView.reloadData()
    }
    func userDidDonate() {
        self.charityDetailTableView?.tableView.reloadData()
    }
    func userDidWriteReview() {
        self.charityDetailTableView?.tableView.reloadData()
    }
    func dataChanged() {
        guard let presenter = presenter else {
            return
        }
        self.charityDetailTableView?.tableView.reloadData()
    }
    
    func addPortfolio() {
        presenter?.addToPortfolio()
    }
    
    func removePortfolio() {
        self.charityDetailTableView?.tableView.reloadData()
    }
}
