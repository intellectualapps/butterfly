//
//  CharityDetailsViewController.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/6/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class CharityDetailsTableViewController: UITableViewController {
    
    var presenter: CharityDetailViewPresenter? {
        didSet {
            tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func add(_ sender: Any) {
        
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return 1
        } else if (section == 1) {
            return 0
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else if (section == 1) {
            return 0
        } else {
            return 30
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = UIView.loadFromNibNamed(nibNamed: "TableViewSectionHeader") as? TableViewSectionHeader else {
            return nil
        }
        
        if section == 0 {
            return nil
        } else if (section == 1) {
            view.lb_title.text = "RECENT DONATION ACTIVITY"
        } else {
            view.lb_title.text = "LATEST REVIEWS"
        }
        return view
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "header", for: indexPath) as? CharityHeaderTableViewCell else {
                return UITableViewCell()
            }
            
            // Configure the cell...
            cell.delegate = self
            cell.presenter = presenter
            
            return cell
        } else if (indexPath.section == 1) {
            return UITableViewCell()
        } else {
            return UITableViewCell()
        }
    }

}

extension CharityDetailsTableViewController: CharityHeaderTableViewCellDelegate {
    func clickDonate() {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "MakeADonationViewController") as? MakeADonationViewController else {
            return
        }
        let navigation = NavigationController(rootViewController: vc)
        vc.charityDetail = presenter?.charity
        self.present(navigation, animated: true, completion: nil)
    }
    
    func clickAddPortfolio() {
        if presenter?.userPortfolio == true {
            presenter?.removeFromPortfolio()
        } else {
            presenter?.addToPortfolio()
        }
    }
}
