//
//  EditPortfolioViewController.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/19/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

class EditPortfolioViewController: HomeViewController {

    override func viewDidLoad() {
        self.addBackButtonDefault()
        tableView.register(UINib(nibName: "CharityDetailTableViewCell", bundle: nil), forCellReuseIdentifier: CharityDetailTableViewCell.identifier)
        
        addTableViewHeader()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlChange), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        presenter = UserPortfolioPresenter(view: self)
        presenter.refresh()
        
    }
    
    func addTableViewHeader() {
        let lb_header = UILabel(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        lb_header.font = UIFont.systemFont(ofSize: 15)
        lb_header.textColor = UIColor.lightGray
        lb_header.textAlignment = .center
        lb_header.text = "Uncheck to remove from your portfolio"
        tableView.tableHeaderView = lb_header
    }

}
