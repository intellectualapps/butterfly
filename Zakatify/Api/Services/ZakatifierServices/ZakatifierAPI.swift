//
//  UserSevicesEndPoint.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

let ZakatifierAPIProvider = MoyaProvider<ZakatifierAPI>(plugins: [NetworkLoggerPlugin(verbose: true,
                                                                          responseDataFormatter: JSONResponseDataFormatter),
                                                      AccessTokenPlugin()])

// MARK: - Provider support

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

public enum ZakatifierAPI {
    case topZakatifiers
}

extension ZakatifierAPI: TargetType {
    public var baseURL: URL { return URL(string: "https://flash-bff-ios.appspot.com/api/v1/zakatifier")! }
    public var path: String {
        switch self {
        case .topZakatifiers:
            return "/top"
        }
    }
    public var method: Moya.Method {
        switch self {
        case .topZakatifiers:
            return .get
        }
    }
    public var parameters: [String: Any]? {
        switch self {
        case .topZakatifiers:
            return nil
        }
    }
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    public var task: Task {
        return .request
    }
    public var validate: Bool {
        return false
    }
    public var sampleData: Data {
         return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
    }
}

private func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}
