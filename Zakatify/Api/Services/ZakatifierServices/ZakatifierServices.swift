//
//  UserServices.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result

/*
 UserServices use to make request to server 
 use :
 - mock
    let service:UserServices = UserServicesMock()
    don't like below
    let serviceMock:UserServicesMock = UserServicesMock()
 when run with real sever we only need change like below
    let service:UserServices = UserServicesReal() // or Any class implement UserServices
 */
protocol ZakatifierServices {
    func topZakatifiers(username: String, page: Int, size: Int, complete:@escaping (_ result:Result<(zakatifiers:[ZakatifyUser],page:Int,size:Int,total:Int),NSError>)-> Void)


}


class ZakatifierServicesCenter: ZakatifierServices {
    func topZakatifiers(username: String, page: Int, size: Int, complete: @escaping (Result<(zakatifiers: [ZakatifyUser], page: Int, size: Int, total: Int), NSError>) -> Void) {
        ZakatifierAPIProvider.request(.topZakatifiers) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let zakatifiers:[ZakatifyUser] = try filtedResponce.mapArray(path: "topZakatifiers")
                    let page: Int = 1
                    let size: Int = 10
                    let total: Int = zakatifiers.count
                    let result = (zakatifiers: zakatifiers, page: page, size: size, total: total)
                    complete(Result.success(result))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

}

