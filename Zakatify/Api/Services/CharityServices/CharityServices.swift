//
//  PaymentServices.swift
//  Paymentify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import Result



protocol CharityServices {
    func suggestList(username: String, page: Int, size: Int, complete:@escaping (_ result:Result<(charities:[Charity],page:Int,size:Int,total:Int),NSError>)-> Void)
    func search(username: String, key: String, page: Int, size: Int, complete:@escaping (_ result:Result<(charities:[Charity],page:Int,size:Int,total:Int),NSError>)-> Void) -> Cancellable
    func addToPortfolio(username: String, charity: CharityDetail, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
    func removeFromPortfolio(username: String, charity: CharityDetail, complete:@escaping (_ result:Result<Bool,NSError>)-> Void)
    func userPortfolio(username: String, page: Int, size: Int, complete:@escaping (_ result:Result<(charities:[Charity],page:Int,size:Int,total:Int),NSError>)-> Void)
}

class CharityServicesCenter: CharityServices {
    
    func suggestList(username: String, page: Int, size: Int = 10, complete: @escaping (Result<(charities: [Charity], page: Int, size: Int, total:Int), NSError>) -> Void) {
        CharityAPIProvider.request(.suggestList(user: username, page: page, size: size)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let charities:[Charity] = try filtedResponce.mapArray(path: "charities")
                    let page: Int = 1
                    let size: Int = 10
                    let total: Int = try filtedResponce.mapValue(path: "totalCount")
                    let result = (charities: charities, page: page, size: size, total: total)
                    complete(Result.success(result))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    func search(username: String, key: String, page: Int, size: Int, complete: @escaping (Result<(charities: [Charity], page: Int, size: Int, total: Int), NSError>) -> Void) -> Cancellable {
        return CharityAPIProvider.request(.search(user: username, query: key, page: page, size: size)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let charities:[Charity] = try filtedResponce.mapArray(path: "charities")
                    let page: Int = try filtedResponce.mapValue(path: "pageNumber")
                    let size: Int = try filtedResponce.mapValue(path: "pageSize")
                    let total: Int = try filtedResponce.mapValue(path: "totalCount")
                    let result = (charities: charities, page: page, size: size, total: total)
                    complete(Result.success(result))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }

    func addToPortfolio(username: String, charity: CharityDetail, complete: @escaping (Result<Bool, NSError>) -> Void) {
        CharityAPIProvider.request(.add(user: username, id: charity.id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let state: Bool = try filtedResponce.mapValue(path: "state")
                    complete(Result.success(state))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    
    func removeFromPortfolio(username: String, charity: CharityDetail, complete:@escaping (_ result:Result<Bool,NSError>)-> Void) {
        CharityAPIProvider.request(.remove(user: username, id: charity.id)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let state: Bool = try filtedResponce.mapValue(path: "state")
                    complete(Result.success(state))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
    
    func userPortfolio(username: String, page: Int, size: Int, complete:@escaping (_ result:Result<(charities:[Charity],page:Int,size:Int,total:Int),NSError>)-> Void) {
        CharityAPIProvider.request(.userPortfolio(user: username, page: page, size: size)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let filtedResponce = try response.filterSuccessfulStatusCodes()
                    let charities:[Charity] = try filtedResponce.mapArray(path: "charities")
                    let page: Int = 1
                    let size: Int = 10
                    let total: Int = try filtedResponce.mapValue(path: "totalCount")
                    let result = (charities: charities, page: page, size: size, total: total)
                    complete(Result.success(result))
                } catch {
                    complete(Result.failure(error as NSError))
                }
                break
            case .failure(let error):
                complete(Result.failure(error as NSError))
                break
            }
        }
    }
}

