//
//  ZakatAPI.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/1/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya

// MARK: - Provider setup

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

let CharityAPIProvider = MoyaProvider<CharityAPI>(plugins: [NetworkLoggerPlugin(verbose: true,
                                                                          responseDataFormatter: JSONResponseDataFormatter),
                                                      AccessTokenPlugin()])

// MARK: - Provider support

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

enum CharityAPI {
    case suggestList(user:String, page: Int, size: Int)
    case search(user:String, query: String, page: Int, size: Int)
    case add(user:String, id: Int)
    case remove(user:String, id: Int)
    case userPortfolio(user:String, page: Int, size: Int)
}

extension CharityAPI: TargetType {
    public var baseURL: URL {
        switch self {
        case .add(user: _, id: _), .remove(user: _, id: _):
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/preference")!
        default:
            return URL(string: "https://flash-bff-ios.appspot.com/api/v1/charity")! }
        }
    public var path: String {
        switch self {
        case .suggestList(user: let username, page: _, size: _):
            return "/suggest/\(username)"
        case .search(user: let username, query: _, page: _, size: _):
            return "/search/\(username)"
        case .add(user: let username, id: _):
            return "/user-charity/\(username)"
        case .remove(user: let username, id: _):
            return "/user-charity/\(username)"
        case .userPortfolio(user: let username, page: _, size: _):
            return "/suggest/\(username)"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .suggestList(user: _, page: _, size: _):
            return .get
        case .search(user: _, query: _, page: _, size: _):
            return .get
        case .add(user: _, id: _):
            return .post
        case .remove(user: _, id: _):
            return .delete
        case .userPortfolio:
            return .get
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
        case .suggestList(user: _, page: let page, size: let size):
            return ["page-number":page,"page-size":size]
        case .search(user: _, query: let key, page: let page, size: let size):
            return ["query":key,"page-number":page,"page-size":size]
        case .add(user: _, id: let id):
            return ["charity-id":id]
        case .remove(user: _, id: let id):
            return ["charity-id":id]
        case .userPortfolio(user: _, page: let page, size: let size):
            return ["page-number":page,"page-size":size]
        }
    }
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    public var task: Task {
        return .request
    }
    public var validate: Bool {
        return false
    }
    public var sampleData: Data {
        return "Half measures are as bad as nothing at all.".data(using: String.Encoding.utf8)!
    }
}
