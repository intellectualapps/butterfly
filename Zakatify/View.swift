//
//  View.swift
//  Zakatify
//
//  Created by Thang Truong on 5/26/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

@IBDesignable

class View: UIView {
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet {
            if cornerRadius == -1 {
                self.layer.cornerRadius = self.bounds.width < self.bounds.height ? self.bounds.width * 0.5 : self.bounds.height * 0.5
            } else {
                self.layer.cornerRadius = cornerRadius
            }
            self.clipsToBounds = true
        }
    }
}
