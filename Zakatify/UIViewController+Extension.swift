//
//  UINavigationController+Extension.swift
//  Zakatify
//
//  Created by Thang Truong on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
// MARK: - contentViewController

extension UIViewController {
    var contentViewController: UIViewController {
        if let navContentVC = self as? UINavigationController {
            return navContentVC.visibleViewController!
        } else {
            return self
        }
    }
}
