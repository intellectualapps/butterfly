//
//  ViewController+InputAccessory.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/10/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit
extension UIViewController {
    open override var inputAccessoryView: UIView? {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.tintColor
        toolBar.isUserInteractionEnabled = true
        let bt_space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let bt_done = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(endEdit))
        toolBar.setItems([bt_space,bt_done], animated: true)
        toolBar.sizeToFit()
        return toolBar
    }
    
    func endEdit() {
        self.view.endEditing(true)
    }
}
