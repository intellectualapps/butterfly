//
//  UIColorExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        let red = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((hex & 0xFF00) >> 8) / 255.0
        let blue = CGFloat((hex & 0xFF)) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }

    class func sportsSouthBlue() -> UIColor {
        return UIColor(hex: 0x00345b)
    }

    class func defaultBackgroundColor() -> UIColor {
        return UIColor(hex: 0x008bb5)
    }
    
    static var blueBorderColor: UIColor {
        return UIColor(hex:0x4CB9FF)
    }
    
    static var navigationBartinColor: UIColor {
        return UIColor(hex:0x3D95CE)
    }
    
    static var navigationTitleColor: UIColor {
        return UIColor.white
    }
    
    static var navigationTintColor: UIColor {
        return UIColor.white
    }
    
    static var buttonBackgroundColor: UIColor {
        return UIColor(hex:0x3D95CE)
    }
    
    static var green: UIColor {
        return UIColor(hex:0x7ED321)
    }
    
    static var tintColor: UIColor {
        return UIColor(hex:0x3D95CE)
    }
}
