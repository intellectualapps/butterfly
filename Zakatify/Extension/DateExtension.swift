//
//  DateExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

extension Date {
    static func date(fromString: String, format: String) -> Date? {
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = format
        return dateFormate.date(from: fromString)
    }
}
