//
//  Logger.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

class Logger {
    class func log(_ msg: String) {
        print(msg)
    }

    class func logFunction(inClass: AnyClass, function: String) {
        Logger.log(NSStringFromClass(inClass) + "." + function)
    }
}
