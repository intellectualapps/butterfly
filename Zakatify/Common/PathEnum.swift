//
//  PathEnum.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
private struct Constants {
    static let baseUrl = Enviroment.env(dev: "https://s.yimg.com/aq/",
                                        stg: "https://s.yimg.com/aq/",
                                        prod: "https://s.yimg.com/aq/")
}

enum Path {
    case Login
    case query
    case queryStock
    case stockDetail

    var relativePath: String {
        switch self {
        case .Login:
            return ""
        case .query:
            return "autoc"
        case .queryStock:
            return "http://autoc.finance.yahoo.com/autoc"
        case .stockDetail:
            return "http://query.yahooapis.com/v1/public/yql"
        }
    }

    var path: String {
        return (NSURL(string: Constants.baseUrl)?.appendingPathComponent(relativePath)?.absoluteString) ?? ""
    }
}
