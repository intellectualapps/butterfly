//
//  EnviromentUtils.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

class Enviroment {
    class func env<T>(dev development: T, stg staging: T, prod production: T) -> T {
        var v: T!

        #if ENVIRONMENT_DEVELOPMENT
            v = development
        #elseif ENVIRONMENT_STAGING
            v = staging
        #else
            v = production
        #endif
        
        return v
    }
}
