//
//  ZakatifyUser.swift
//  Zakatify
//
//  Created by Tran Vuong Minh on 6/24/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper

class ZakatifyUser: Mappable {
    enum JSONKey: String {
        case donationProgress
        case user
    }
    var user: ZakatifierInfo = ZakatifierInfo()
    var donationProgress: DonationProgess = UserDonationProgess()
    init() {

    }

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        user <- map[JSONKey.user.rawValue]
        donationProgress <- map[JSONKey.donationProgress.rawValue]
    }
}
