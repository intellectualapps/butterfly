//
//  Tag.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import RandomKit

protocol Tag {
    var id: Int {get set}
    var description: String {get set}
}

class TagMock: Tag {
    var id: Int = 0
    var description: String = ""
    init() {
        if let value = ["media", "Education", "relief", "foodscurity", "freedom"].random(using: &Xoroshiro.default) {
            self.description = value
        }
    }
}
