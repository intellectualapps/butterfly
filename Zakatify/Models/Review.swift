//
//  Review.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import RandomKit

protocol Review {
    var reviewer: String {get set}
    var content: String {get set}
}

class ReviewMock: Review {
    var reviewer: String = ""
    var content: String = ""
    init() {
        if let value = ["Bob", "Cindy", "May", "Charles", "Javier"].random(using: &Xoroshiro.default) {
            self.reviewer = value
        }
        let contentArr = ["Program Expenses: The majority of charities listed on our site - seven out of ten non profits - spend at least 75% of their expenses directly on their programs. That means the organization should spend no more than 25% of their total expenses on administrative overhead and fundraising costs combined. To determine the percentage going to programs for the charity you are reviewing, scroll to page 10 (Statement of Functional Expenses), find Line 25 (total functional expenses). Divide column B (program services) by column A (total expenses) then multiply by 100. The resulting figure is the percentage that organization is spending directly on their programs and services. For a more detailed break out of the program expenses, review the \"Statement of Program Service Accomplishments\" located on Page 2, Part III.",
                        "Growth of Program Expenses: Determine if the charity you are considering supporting is expanding or shrinking over time. You can quickly do this by comparing the Total Program Expenses- page 10, line 25B of the current year with the prior year(s). While the growth doesn't need to be dramatic, charities that are shrinking are very likely cutting the very programs that you want to support.",
                        "Types of Support: Take a look at lines 1a through 1g (on page 9, \"Statement of Revenue\") to learn about a charity's funding sources. Some charities rely heavily on membership dues (line 1b), or government support (line 1e) while others survive almost solely on individual contributions and fundraisers (line 1f; 1c) and still others depend on program service revenue (line 2g). Having multiple sources of revenue can be beneficial for a charity. For example, if an organization experiences a drop in donations from individuals, then it can draw from other revenue sources to sustain its programs. If a charity has no revenue listed on line 1f, then it may not even be prepared to accept private contributions.",
        "Executive Pay: Our research shows that the average CEO compensation is in the low to mid six figures among the charities that we rate. On page 7 of the Form 990 (Compensation of Officers, Directors, etc.) organizations are required to report the CEO's pay and any current officers making over $100,000 annually.  As you examine salaries, keep in mind that a variety of factors impact pay including geographic location, size of the organization, and type of work performed.",
        "Professional Fundraisers: You can determine if a charity uses professional fundraisers by examining the charity's Form 990 in Part I, line 16a, column b and in Schedule G (which offers a more detailed breakdown). Use of professional fundraisers in and of itself is not necessarily a bad thing. Professional fundraisers can be more efficient and effective at raising funds for the charity than staff or volunteers would be and some of the costs spent on these third party companies are funds that would also have to be spent on staff, equipment, and technology if the campaign were entirely managed in-house as well. However, if the charity is spending a lot on outside fundraising firms with little going towards its charitable mission, then you may want to look for another charity to support. For more information on this topic, see our tips for What To Do When A Charity Calls and our  Top 10 list of Charities Overpaying their For-Profit Fundraisers."]
        
        if let value = contentArr.random(using: &Xoroshiro.default) {
            self.content = value
        }
    }
}
