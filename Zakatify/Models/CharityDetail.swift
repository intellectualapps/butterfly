//
//  CharityDetail.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import ObjectMapper
import RandomKit

protocol CharityDetail {
    var id: Int {get set}
    var newfeeds: [Feed] {get set}
    var name: String {get set}
    var description: String {get set}
    var logoUrl: String {get set}
    var rate: Int {get set}
    var totalMoney: Int {get set}
    var donors: [Donor] {get set}
    var reviews: [Review] {get set}
    var tags: [Tag] {get set}
    var added: Bool {get set}
    var ein: String {get set}
    var phoneNumber: String {get set}
    var reportUrl: String {get set}
    var address: String {get set}
}

class Charity: CharityDetail, Mappable {
    enum JSONKey: String {
        case id = "charityId"
        case newFeeds = "newsFeed"
        case name = "charityName"
        case description = "description"
        case logoUrl = "charityLogo"
        case rate
        case totalMoney = "totalDonation"
        case donors = "donors"
        case reviews
        case tags
        case added = "selected"
        case ein = "ein"
        case phoneNumber = "phoneNumber"
        case reportUrl = "reportUrl"
        case address = "address"
    }
    var id: Int = 0
    var newfeeds: [Feed] = []
    var name: String = ""
    var description: String = ""
    var logoUrl: String = ""
    var rate: Int = 0
    var totalMoney: Int = 0
    var donors: [Donor] = []
    var reviews: [Review] = []
    var tags: [Tag] = []
    var added: Bool = false
    var ein: String = ""
    var phoneNumber: String = ""
    var reportUrl: String = ""
    var address: String = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map[JSONKey.id.rawValue]
        newfeeds <- map[JSONKey.newFeeds.rawValue]
        name <- map[JSONKey.name.rawValue]
        description <- map[JSONKey.description.rawValue]
        logoUrl <- map[JSONKey.logoUrl.rawValue]
        rate <- map[JSONKey.rate.rawValue]
        totalMoney <- map[JSONKey.totalMoney.rawValue]
        donors <- map[JSONKey.donors.rawValue]
        reviews <- map[JSONKey.reviews.rawValue]
        tags <- map[JSONKey.tags.rawValue]
        added <- map[JSONKey.added.rawValue]
        ein <- map[JSONKey.ein.rawValue]
        phoneNumber <- map[JSONKey.phoneNumber.rawValue]
        reportUrl <- map[JSONKey.reportUrl.rawValue]
        address <- map[JSONKey.address.rawValue]
    }
    
}

class CharityDetailMock: CharityDetail {
    var id: Int = 0
    var newfeeds: [Feed] = []
    var name: String = ""
    var description: String = ""
    var logoUrl: String = ""
    var rate: Int = 0
    var totalMoney: Int = 0
    var donors: [Donor] = []
    var reviews: [Review] = []
    var tags: [Tag] = []
    var added: Bool = false
    var ein: String = ""
    var phoneNumber: String = ""
    var reportUrl: String = ""
    var address: String = ""
    init() {
        if let value = ["shahed just donated to",
                       ""].random(using: &Xoroshiro.default) {
            self.newfeeds = [Feed(activityType: "donated", activityBy: "shahed")]
        }
        
        if let name = ["NAACP Legal Defense and Educational Fund",
                       "National Urban League (National Office)",
                       "American Foundation for Children with AIDS",
                       "American Sexual Health Association",
                       "Elizabeth Glaser Pediatric AIDS Foundation"].random(using: &Xoroshiro.default) {
            self.name = name
        }
        
        if let name = ["NAACP Legal Defense and Educational Fund",
                       "National Urban League (National Office)",
                       "American Foundation for Children with AIDS",
                       "American Sexual Health Association",
                       "Elizabeth Glaser Pediatric AIDS Foundation"].random(using: &Xoroshiro.default) {
            self.description = name
        }
        
        self.logoUrl = "https://www.rethink.org/images/logo.png"
        
        if let rate = Int.random(within: 0 ..< 5, using: &Xoroshiro.default) {
            self.rate = rate
        }
        
        if let value = Int.random(within: 0 ..< 1000000, using: &Xoroshiro.default) {
            self.totalMoney = value
        }
        
        if let value = Int.random(within: 0 ..< 30, using: &Xoroshiro.default) {
            var arr = [Zakatifier]()
//            for _ in 0...value {
//                let donor = ZakatifierMock()
//                arr.append(donor)
//            }
//            self.donors = arr
        }
        
        if let value = Int.random(within: 0 ..< 30, using: &Xoroshiro.default) {
            var arr = [Review]()
            for _ in 0...value {
                let review = ReviewMock()
                arr.append(review)
            }
            self.reviews = arr
        }
        
        if let value = Int.random(within: 0 ..< 5, using: &Xoroshiro.default) {
            var arr = [Tag]()
            for _ in 0...value {
                let mock = TagMock()
                arr.append(mock)
            }
            self.tags = arr
        }
    }
    
    func randomName() {
        
    }
    
    func randomRate() {
        
    }
    
    func randomTotalMoney() {
        
    }
    
    func randomDonors() {
        
    }
    
    func randomReviews() {
        
    }
}
