//
//  Donation.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import RandomKit

protocol Donation {
    var charityName: String {get set}
    var money: Int {get set}
    var total: Int {get set}
}

class DonationMock: Donation {
    var charityName: String = ""
    var money: Int = 0
    var total: Int = 0
    
    init() {
        if let value = ["NAACP Legal Defense and Educational Fund",
         "National Urban League (National Office)",
         "American Foundation for Children with AIDS",
         "American Sexual Health Association",
         "Elizabeth Glaser Pediatric AIDS Foundation"].random(using: &Xoroshiro.default) {
            self.charityName = value
        }
        
        if let value = Int.random(within: 0 ..< 1000, using: &Xoroshiro.default) {
            self.money = value
        }
        
        if let value = Int.random(within: money ..< money * 10, using: &Xoroshiro.default) {
            self.total = value
        }
    }
}

