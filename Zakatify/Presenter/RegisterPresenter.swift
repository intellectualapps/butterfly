//
//  RegisterPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/28/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import Moya
import FluidValidator

class Length: BeNotEmpty {
    var min = 0
    var max = 10
    
    
    public init(min:Int,max:Int) {
        self.min = min
        self.max = max
        super.init()
        self.overrideErrorMessage = "This must be in \(min) to \(max) characters"
    }
    
    override open func performValidation(onObject object: AnyObject?) -> Bool {
        if(!super.performValidation(onObject: object)) {
            return false
        }
        if let string = object as? String {
            if string.characters.count < min || string.characters.count > max {
                return false
            }
        }
        return true
    }
}

private class UserValidator: AbstractValidator<UserInfo> {
    override init() {
        super.init()
        self.addValidation(withName: "password") { (user) -> (Any?) in
            user.password
            }.addRule(Length(min: 6, max: 15))
        self.addValidation(withName: "email") { (user) -> (Any?) in
            user.email
            }.addRule(ValidEmail())
        self.addValidation(withName: "username") { (user) -> (Any?) in
            user.username
            }.addRule(Length(min: 3, max: 20))
    }
}

protocol RegisterView: CommonView {
    func registerSuccess(user:UserInfo)
    func showLoadingVerify()
    func hideLoadingVerify()
    func verify(username:String, status:Bool)
}

protocol RegisterViewPresenter {
    init(view:RegisterView)
    func verify(username:String)
    func register(username:String, email:String, password:String, repeatPassword:String)
}

class RegisterPresenter: RegisterViewPresenter {
    
    unowned let view: RegisterView
    let service:UserServices = UserServicesCenter()
    
    var verifyReqest: Cancellable?

    required init(view: RegisterView) {
        self.view = view
    }
    
    func verify(username: String) {
        verifyReqest?.cancel()
        view.showLoadingVerify()
        verifyReqest = service.verify(username) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.verifyReqest = nil
            strongSelf.view.hideLoadingVerify()
            switch result {
            case .success(let status):
                if status {
                    
                } else {
                    strongSelf.view.showAlert("The username you selected is unavailable. Please try another one.")
                }
            case .failure(let error):
                if error.code == 4 {
                    return
                }
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }

    func register(username: String, email: String, password: String, repeatPassword: String) {
        let user = UserInfo()
        user.username = username
        user.email = email
        user.password = password
        let validator = UserValidator()
        let _ = validator.validate(object: user)
        let failMessage = validator.allErrors
        if let key = validator.allErrors.failingFields().first {
            if let message = failMessage.failMessageForPath(key)?.errors.first?.compact {
                self.view.showAlert(message.replacingOccurrences(of: "This", with: "\(key)"))
            }
            return
        }
        
        if password != repeatPassword {
            self.view.showAlert("password and repeat password not match")
            return
        }
        
        view.showLoading()
        service.register(username, email: email, password: password, repeatPassword: password) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let user):
                UserManager.shared.currentUser = user
                UserManager.shared.authenToken = user.authToken
                strongSelf.view.registerSuccess(user: user)
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
}
