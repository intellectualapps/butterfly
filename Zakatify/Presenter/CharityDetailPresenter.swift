//
//  CharityPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import UIKit

protocol CharityDetailView: CommonView {
    func userDidAddToPorfolio()
    func userDidDonate()
    func userDidWriteReview()
    func dataChanged()
}

protocol CharityDetailViewPresenter {
    var view: CharityDetailView {get set}
    var charity: CharityDetail {get set}
    init(view:CharityDetailView, model:CharityDetail)
    func donate(money:Int, payment: Payment)
    func addToPortfolio()
    func removeFromPortfolio()
    func refresh()
    var charityNewFeed: NSAttributedString {get}
    var charityLogoUrl: String {get}
    var charityName: String {get}
    var charityRate: Int {get}
    var charityDesription: String {get}
    var donateDescription: String {get}
    var moneyDescription: String {get}
    var donorsAvatarUrls:[URL] {get}
    var tagDescription: String {get}
    var userPortfolio: Bool {get}
}

extension CharityDetailViewPresenter {
    var newFeedIcon: NSAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "donated-green")
        let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
        return attachmentString
    }
    var charityNewFeed: NSAttributedString {
        if charity.newfeeds.count == 0 {
            return NSAttributedString(string: "")
        }
        let att = NSMutableAttributedString(attributedString: newFeedIcon)
        att.append(NSAttributedString(string: " "))
        att.append(NSMutableAttributedString(string: charity.newfeeds.first?.description ?? ""))
        return att
    }
    var charityLogoUrl: String {
        return charity.logoUrl
    }
    var charityName: String {
        return charity.name
    }
    var charityRate: Int {
        return charity.rate
    }
    var charityDesription: String {
        return charity.description
    }
    var donateDescription: String {
        return "+\(charity.donors.count) donated"
    }
    var moneyDescription: String {
        return "$\(charity.totalMoney)"
    }
    var donorsAvatarUrls:[URL] {
        var urls = [URL]()
        for donor in charity.donors where urls.count < 5 {
            if let url = URL(string:donor.profileUrl) {
                urls.append(url)
            }
        }
        return urls
    }
    var tagDescription: String {
        return charity.tags.reduce("", { (result, tag) -> String in
            return "\(result)  #\(tag.description)"
        })
    }
    
    var userPortfolio: Bool {
        return charity.added
    }
}

class CharityDetailPresenter: CharityDetailViewPresenter {
    
    enum Notification: String {
        case added
        case remove
    }

    unowned var view: CharityDetailView
    var charity: CharityDetail
    let service: CharityServices = CharityServicesCenter()
    
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    
    required init(view: CharityDetailView, model: CharityDetail) {
        self.view = view
        self.charity = model
    }
    
    func donate(money: Int, payment: Payment) {
        if money <= 0 {
            view.showAlert("please input number more than zero")
        }
    }
    
    func addToPortfolio() {
        view.showLoading()
        service.addToPortfolio(username: username, charity: self.charity) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let state):
                strongSelf.charity.added = true
                NotificationCenter.default.post(name: NSNotification.Name(Notification.added.rawValue), object: nil)
                strongSelf.view.userDidAddToPorfolio()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    func removeFromPortfolio() {
        view.showLoading()
        service.removeFromPortfolio(username: username, charity: self.charity) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let state):
                strongSelf.charity.added = false
                NotificationCenter.default.post(name: NSNotification.Name(Notification.remove.rawValue), object: nil)
                strongSelf.view.userDidAddToPorfolio()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
        }
    }
    
    func refresh() {
        
    }
}

class CharityDetailPresenterMock: CharityDetailViewPresenter {
    unowned var view: CharityDetailView
    var charity: CharityDetail
    
    required init(view: CharityDetailView, model: CharityDetail) {
        self.view = view
        self.charity = model
    }

    func donate(money: Int, payment: Payment) {
        if money <= 0 {
            view.showAlert("please input number more than zero")
        }
    }

    func addToPortfolio() {
        
    }
    
    func removeFromPortfolio() {
        
    }

    func refresh() {
        
    }
    
}
