//
//  CharityListPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 5/30/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import RandomKit
import Moya

protocol CharityListViewPresenter: TableViewPresenter {
    var charities: [CharityDetail] {get set}
    var view:TableView {get}
    init(view: TableView)
    func charity(index:Int) -> CharityDetail?
    func add(charity:CharityDetail)
    func remove(charity:CharityDetail)
}

class CharityListPresenter: CharityListViewPresenter {
    var charities: [CharityDetail] = []
    unowned var view: TableView
    var pageCount: Page = PageCount()
    let service: CharityServices = CharityServicesCenter()
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    
    required init(view: TableView) {
        self.view = view
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationChange),
                                               name: NSNotification.Name(CharityDetailPresenter.Notification.added.rawValue),
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(notificationChange),
                                               name: NSNotification.Name(CharityDetailPresenter.Notification.remove.rawValue),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return charities.count
    }
    
    @objc func notificationChange() {
        view.refreshed()
    }
    
    var searchCancelable: Cancellable?
    func refresh(search: String? = nil) {
        guard let search = search else {
            return
        }
        if search.isEmpty {
            return
        }
        if searchCancelable?.cancel() != nil {
            view.refreshed()
        }
        view.refreshing()
        searchCancelable = service.search(username: username, key: search, page: pageCount.page, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((charities:let charities, page:let page ,size:let size, total:let total)):
                strongSelf.charities = charities
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
            case .failure(let error):
                if error.code == 4 {
                    return
                }
                strongSelf.view.showAlert(error.localizedDescription)
                strongSelf.charities = []
            }
            strongSelf.view.refreshed()
        }
    }
    
    func refresh() {
        view.refreshing()
        service.suggestList(username: username, page: pageCount.page, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((charities:let charities, page:let page ,size:let size, total:let total)):
                strongSelf.charities = charities
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }
    }
    
    func loadmore() {
        if shouldLoadmore() == false {
            return
        }
        view.refreshing()
        service.suggestList(username: username, page: pageCount.page + 1, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((charities:let charities, page:let page ,size:let size, total:let total)):
                strongSelf.charities = charities
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }
    }
    func shouldLoadmore() -> Bool {
        return charities.count < pageCount.total
    }
    
    func charity(index: Int) -> CharityDetail? {
        if index < charities.count {
            return charities[index]
        }
        return nil
    }
    
    func add(charity:CharityDetail) {
        let index = charities.index { (element) -> Bool in
            return element.id == charity.id
        }
        view.showLoading()
        service.addToPortfolio(username: username, charity: charity) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let state):
                if let index = index {
                    self?.charities[index].added = true
                }
                strongSelf.view.refreshed()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }
    }
    
    func remove(charity:CharityDetail) {
        let index = charities.index { (element) -> Bool in
            return element.id == charity.id
        }
        view.showLoading()
        service.removeFromPortfolio(username: username, charity: charity) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success(let state):
                if let index = index {
                    self?.charities[index].added = false
                }
                strongSelf.view.refreshed()
            case .failure(let error):
                strongSelf.view.showAlert(error.localizedDescription)
            }
            strongSelf.view.refreshed()
        }
    }
}

class CharityListPresenterMock: CharityListViewPresenter {
    var charities: [CharityDetail] = []
    unowned var view: TableView
    required init(view: TableView) {
        self.view = view
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return charities.count
    }
    func refresh(search: String? = nil) {
        view.refreshing()
        if let value = Int.random(within: 0 ..< 10, using: &Xoroshiro.default) {
            var arr = [CharityDetail]()
            for _ in 0...value {
                let mock = CharityDetailMock()
                arr.append(mock)
            }
            self.charities = arr
        }
        self.view.refreshed()
    }
    
    func refresh() {
        view.refreshing()
        if let value = Int.random(within: 0 ..< 10, using: &Xoroshiro.default) {
            var arr = [CharityDetail]()
            for _ in 0...value {
                let mock = CharityDetailMock()
                arr.append(mock)
            }
            self.charities = arr
        }
        self.view.refreshed()
    }
    
    func loadmore() {
        view.loadingMore()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            if let value = Int.random(within: 0 ..< 10, using: &Xoroshiro.default) {
                var indexPaths = [IndexPath]()
                let oldIndex = self.charities.count - 1
                for index in 0...value {
                    let mock = CharityDetailMock()
                    let indexPath = IndexPath(row: index + oldIndex + 1, section: 0)
                    indexPaths.append(indexPath)
                    self.charities.append(mock)
                }
            }
            self.view.loadedMore()
        })
    }
    func shouldLoadmore() -> Bool {
        return true
    }
    
    func charity(index: Int) -> CharityDetail? {
        if index < charities.count {
            return charities[index]
        }
        return nil
    }
    
    func add(charity:CharityDetail) {
        
    }
    
    func remove(charity:CharityDetail) {
        
    }
}
