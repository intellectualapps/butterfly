//
//  ListZakatifiersPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/8/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import RandomKit

protocol ListZakatifiersView: TableView {
    
}

protocol ListZakatifiersViewPresenter: TableViewPresenter {
    var view: ListZakatifiersView {get set}
    init(view: ListZakatifiersView)
    func addZakatifier(_ zakatifier: ZakatifyUser)
    func zakatifer(at index:IndexPath) -> ZakatifyUser?
}

class ListZakatifiersPresenter: ListZakatifiersViewPresenter {
    unowned var view: ListZakatifiersView
    var zakatifiers: [ZakatifyUser] = []
    var username: String {
        if let user = UserManager.shared.currentUser?.username {
            return user
        }
        return ""
    }
    var pageCount: Page = PageCount()
    var services: ZakatifierServices = ZakatifierServicesCenter()

    required init(view: ListZakatifiersView) {
        self.view = view
    }

    func numberOfSections() -> Int {
        return 1
    }
    func numberOfRowsInSection(section:Int) -> Int {
        return zakatifiers.count
    }
    func refresh(search: String?) {
        view.refreshed()
        services.topZakatifiers(username: username, page: pageCount.page, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((zakatifiers:let zakatifiers, page:let page ,size:let size, total:let total)):
                strongSelf.zakatifiers = zakatifiers
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
            case .failure(let error):
                if error.code == 4 {
                    return
                }
                strongSelf.view.showAlert(error.localizedDescription)
                strongSelf.zakatifiers = []
            }
            strongSelf.view.refreshed()
        }
    }
    func refresh() {
        view.refreshed()
        services.topZakatifiers(username: username, page: pageCount.page, size: pageCount.size) { [weak self] (result) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.hideLoading()
            switch result {
            case .success((zakatifiers:let zakatifiers, page:let page ,size:let size, total:let total)):
                strongSelf.zakatifiers = zakatifiers
                strongSelf.pageCount = PageCount(page: page, size: size, total: total)
            case .failure(let error):
                if error.code == 4 {
                    return
                }
                strongSelf.view.showAlert(error.localizedDescription)
                strongSelf.zakatifiers = []
            }
            strongSelf.view.refreshed()
        }
    }
    func loadmore() {
//        view.loadedMore()
    }
    func shouldLoadmore() -> Bool {
        return false
    }
    func addZakatifier(_ zakatifier: ZakatifyUser) {
        
    }
    func zakatifer(at index:IndexPath) -> ZakatifyUser? {
        if index.row < zakatifiers.count {
            return zakatifiers[index.row]
        }
        return nil
    }
}
