//
//  ZakatifierPresenter.swift
//  Zakatify
//
//  Created by tran vuong minh on 6/8/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

protocol ZakatifierDetailsView: CommonView {
    
}

protocol ZakatifierDetailsViewPresenter {
    var view:ZakatifierDetailsView {get set}
    init(view:ZakatifierDetailsView, model: ZakatifyUser)
    func sendMessage(message:String)
    func follow()
}

class ZakatifierDetailsPresenter: ZakatifierDetailsViewPresenter {
    unowned var view: ZakatifierDetailsView
    var zakatifier: ZakatifyUser
    required init(view: ZakatifierDetailsView, model: ZakatifyUser) {
        self.view = view
        self.zakatifier = model
    }

    func sendMessage(message:String) {
        view.showLoading()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.view.hideLoading()
        })
    }
    
    func follow() {
        view.showLoading()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.view.hideLoading()
        })
    }
    
}
