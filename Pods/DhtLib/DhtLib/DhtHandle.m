//
//  DhtHandle.m
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/31/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import "DhtHandle.h"
#import "DhtUtility.h"

@interface DhtHandle() {
    __SYNCHRONIZED_DEFINE(_delegate);
}
@end
@implementation DhtHandle

- (id)initWithDelegate:(id<DhtWatcher>)delegate willReleaseOnMainThread:(bool)willReleaseOnMainThread {
    self = [super init];
    if (self != nil) {
        __SYNCHRONIZED_INIT(_delegate);
        _delegate = delegate;
        _willReleaseOnMainThread = willReleaseOnMainThread;
    }
    return self;
}

- (void) setDelegate:(id<DhtWatcher>)delegate {
    __SYNCHRONIZED_BEGIN(_delegate);
    _delegate = delegate;
    __SYNCHRONIZED_END(_delegate);
}

- (bool)hasDelegate {
    bool result = false;
    __SYNCHRONIZED_BEGIN(_delegate);
    result = _delegate != nil;
    __SYNCHRONIZED_END(_delegate);
    return result;
}

- (void)reset {
    _delegate = nil;
}

- (void)receiveActorMessage:(NSString *)path messageType:(NSString *)messageType message:(id)message {
    __strong id<DhtWatcher> delegate = self.delegate;
    if (delegate != nil && [delegate respondsToSelector:@selector(actorMessageReceived:messageType:message:)])
        [delegate actorMessageReceived:path messageType:messageType message:message];
    
    if (_willReleaseOnMainThread && ![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [delegate class];
        });
    }
}

- (void)handleNotifyResourceDispatched:(NSString *)path resource:(id)resource arguments:(id)arguments {
    __strong id<DhtWatcher> delegate = self.delegate;
    if (delegate != nil && [delegate respondsToSelector:@selector(actionNotifyResourceDispatched:resource:arguments:)])
        [delegate actionNotifyResourceDispatched:path resource:resource arguments:arguments];
    
    if (_willReleaseOnMainThread && ![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [delegate class];
        });
    }
}

- (void)requestAction:(NSString *)action options:(id)options {
    __strong id <DhtWatcher> __delegate= self.delegate;
    if (__delegate != nil && [__delegate respondsToSelector:@selector(actionStageActionRequested:options:)])
        [__delegate actionStageActionRequested:action options:options];
    
    if (_willReleaseOnMainThread && ![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [__delegate class];
        });
    }
}

@end
