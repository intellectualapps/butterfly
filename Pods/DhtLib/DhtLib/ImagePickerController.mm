//
//  ImagePickerController.m
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "ImagePickerController.h"
#import "DhtRequestTimer.h"
#import "Utils.h"
#import "DhtObserverProxy.h"
#import "BackToolbarButton.h"
#import "AssetHolder.h"
#import "DhtActionControl.h"

#pragma mark -



static ALAssetsLibrary *sharedLibrary = nil;

static const char *assetsProcessingQueueSpecific = "assetsProcessingQueue";
static int sharedLibraryRetainCount = 0;
static DhtRequestTimer *sharedLibraryReleaseTimer = nil;
static dispatch_queue_t assetsProcessingQueue()
{
    static dispatch_queue_t queue = NULL;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      queue = dispatch_queue_create("com.tg.assetsqueue", 0);
                      if (&dispatch_queue_set_specific != NULL)
                          dispatch_queue_set_specific(queue, assetsProcessingQueueSpecific, (void *)assetsProcessingQueueSpecific, NULL);
                  });
    
    return queue;
}


void dispatchOnAssetsProcessingQueue(dispatch_block_t block)
{
    bool isCurrentQueueAssetsProcessingQueue = false;
    isCurrentQueueAssetsProcessingQueue = dispatch_get_specific(assetsProcessingQueueSpecific) != NULL;
    
    if (isCurrentQueueAssetsProcessingQueue)
        block();
    else
        dispatch_async(assetsProcessingQueue(), block);
}
void sharedAssetsLibraryRetain()
{
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        if (sharedLibraryReleaseTimer != nil)
                                        {
                                            [sharedLibraryReleaseTimer invalidate];
                                            sharedLibraryReleaseTimer = nil;
                                        }
                                        
                                        if (sharedLibrary == nil)
                                        {
                                            [KLogger log: @"Preloading shared assets library"];
                                            sharedLibraryRetainCount = 1;
                                            sharedLibrary = [[ALAssetsLibrary alloc] init];
                                            NSInteger ossversion = [[[UIDevice currentDevice] systemVersion] intValue];
                                            if (ossversion == 5)
                                                [sharedLibrary writeImageToSavedPhotosAlbum:nil metadata:nil completionBlock:^(__unused NSURL *assetURL, __unused NSError *error) { }];
                                            
                                            [sharedLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop)
                                             {
                                                 if (group != nil)
                                                 {
                                                     if (stop != NULL)
                                                         *stop = true;
                                                     
                                                     [group setAssetsFilter:[ALAssetsFilter allPhotos]];
                                                     [group numberOfAssets];
                                                 }
                                             } failureBlock:^(__unused NSError *error)
                                             {
                                                 [KLogger log: @"assets access error"];
                                             }];
                                        }
                                        else
                                            sharedLibraryRetainCount++;
                                    });
}

void sharedAssetsLibraryRelease()
{
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        sharedLibraryRetainCount--;
                                        if (sharedLibraryRetainCount <= 0)
                                        {
                                            sharedLibraryRetainCount = 0;
                                            
                                            if (sharedLibraryReleaseTimer != nil)
                                            {
                                                [sharedLibraryReleaseTimer invalidate];
                                                sharedLibraryReleaseTimer = nil;
                                            }
                                            
                                            sharedLibraryReleaseTimer = [[DhtRequestTimer alloc] initWithTimeout:4 repeat:false completion:^
                                                                         {
                                                                             sharedLibraryReleaseTimer = nil;
                                                                             [KLogger log: @"Destroyed shared assets library"];
                                                                             sharedLibrary = nil;
                                                                         } queue:assetsProcessingQueue()];
                                            [sharedLibraryReleaseTimer start];
                                        }
                                    });
}


@interface AssetsLibraryHolder : NSObject

@end

@implementation AssetsLibraryHolder

- (void)dealloc
{
    sharedAssetsLibraryRelease();
}

@end



@interface ImagePickerController() <UITableViewDataSource, UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    volatile bool _stopAllTasks;
    
    CGFloat _inset;
}
@property (nonatomic, strong) NSString *hideItemUrl;
@property (nonatomic) int assetsInRow;
@property (nonatomic) CGFloat imageSize;
@property (nonatomic) CGFloat lineHeight;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property (nonatomic, strong) ALAssetsGroup *assetsGroup;
@property (nonatomic, strong) NSURL *groupUrl;
@property (nonatomic, strong) NSString *groupTitle;
@property (nonatomic) bool avatarSelectionMode;

@property (nonatomic) bool navigationBarShouldBeHidden;

@property (nonatomic) bool autoManageStatusBarBackground;
@property (nonatomic) bool automaticallyManageScrollViewInsets;
@property (nonatomic) bool ignoreKeyboardWhenAdjustingScrollViewInsets;

@property (nonatomic, strong) NSMutableArray *assetList;
@property (nonatomic, strong) DhtObserverProxy *assetsLibraryDidChangeProxy;
@property (nonatomic, strong) UIBarButtonItem *closeButtonItem;
@property (nonatomic, strong) UIBarButtonItem *doneButtonItem;
@property (nonatomic, strong) DhtRequestTimer *assetsChangeDelayTimer;
@property (nonatomic, strong) UIButton *cancelButton;

@end

@implementation ImagePickerController

@synthesize tableView = _tableView;


+ (id)sharedAssetsLibrary
{
    return sharedLibrary;
}

+ (id)preloadLibrary
{
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        if ([(id)[ALAssetsLibrary class] respondsToSelector:@selector(authorizationStatus)])
                                        {
                                            if ([ALAssetsLibrary authorizationStatus] != ALAuthorizationStatusAuthorized)
                                                return;
                                        }
                                        
                                        sharedAssetsLibraryRetain();
                                    });
    
    AssetsLibraryHolder *libraryHolder = [[AssetsLibraryHolder alloc] init];
    return libraryHolder;
}
+ (void)loadAssetWithUrl:(NSURL *)url completion:(void (^)(ALAsset *asset))completion
{
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        if (sharedLibrary != nil)
                                        {
                                            [sharedLibrary assetForURL:url resultBlock:^(ALAsset *asset)
                                             {
                                                 if (completion)
                                                     completion(asset);
                                             } failureBlock:^(__unused NSError *error)
                                             {
                                                 if (completion)
                                                     completion(nil);
                                             }];
                                        }
                                        else
                                        {
                                            if (completion)
                                                completion(nil);
                                        }
                                    });
}

+ (void)storeImageAsset:(NSData *)data
{
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        ALAssetsLibrary *library = sharedLibrary;
                                        if (library == nil)
                                            library = [[ALAssetsLibrary alloc] init];
                                        
                                        [library writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:nil];
                                    });
}

- (id)initWithGroupUrl:(NSURL *)groupUrl groupTitle:(NSString *)groupTitle avatarSelection:(bool)avatarSelection
{
    self = [super initWithNibName:nil bundle:nil];
    if (self)
    {
        _groupUrl = groupUrl;
        _groupTitle = groupTitle;
        _avatarSelectionMode = avatarSelection;
        
        [self commonInit];
    }
    return self;
}


- (void)commonInit
{
    self.automaticallyManageScrollViewInsets = false;
    
    _stopAllTasks = false;
    _handler = [[DhtHandle alloc] initWithDelegate:self willReleaseOnMainThread:true];
    
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        sharedAssetsLibraryRetain();
                                        _assetsLibrary = sharedLibrary;
                                    });
    
    _assetsLibraryDidChangeProxy = [[DhtObserverProxy alloc] initWithTarget:self targetSelector:@selector(assetsLibraryDidChange:) name:ALAssetsLibraryChangedNotification object:_assetsLibrary];
}

- (void) loadView {
    [super loadView];
    if (_groupTitle == nil) {
        self.title = @"Photos";
    } else {
        self.title = _groupTitle;
    }
    self.view.backgroundColor = [UIColor whiteColor];
    CGSize screenSize = ScreenSize();
    _assetsInRow = [self assetsInRowForWidth:screenSize.width widescreenWidth:screenSize.width];
    _imageSize = [self imageSizeForWidth:screenSize.width widescreenWidth:screenSize.width];
    _lineHeight = [self lineHeightForWidth:screenSize.width widescreenWidth:screenSize.width];
    _inset = [self insetForWidth:screenSize.width widescreenWidth:screenSize.width];
    CGRect tableFrame = CGRectMake(0, 0, screenSize.width, screenSize.height); // 64 navigation header height
    _tableView = [[UITableView alloc] init];
    _tableView.frame = tableFrame;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.scrollIndicatorInsets = UIEdgeInsetsMake(_tableView.contentInset.top, 0, 0, 0);
    [self.view addSubview:_tableView];
    [self updateSelectionInterface:false];
    [self reloadAssets:true];
    self.navigationItem.backBarButtonItem = nil;
    self.navigationItem.leftBarButtonItem = [self closeButtonItem];
    self.navigationItem.rightBarButtonItem = [self doneButtonItem];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [KLogger log: [NSString stringWithFormat: @"asset selected count = %lu",(unsigned long)assetHolder().selectedAssets.count]];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super  viewDidDisappear:animated];
}

- (UIBarButtonItem *)closeButtonItem
{
    if (_closeButtonItem == nil)
    {
        BackToolbarButton *backButton = [[BackToolbarButton alloc] initWithLightModeAndimage:nil];
        backButton.arrowOffset = -1;
        [backButton addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
        UILabel *backLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
        backLbl.textColor = [UIColor blackColor];
        backLbl.font = [UIFont fontWithName:@"Quicksand-Bold" size:16];
        backButton.buttonTitleLabel = backLbl;
        [backButton addSubview:backLbl];
        [backButton setButtonTitle:@"Albums"];
        [backButton sizeToFit];
        _closeButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    }
    
    return _closeButtonItem;
}

- (UIBarButtonItem *)doneButtonItem
{
    if (_doneButtonItem == nil)
    {
        UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [doneButton setTitle:NSLocalizedString(@"button.done", nil) forState:UIControlStateNormal];
        doneButton.titleLabel.font = [UIFont fontWithName:@"Quicksand-Bold" size:16];
        [doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [doneButton sizeToFit];
        [doneButton addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        _closeButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    }
    
    return _closeButtonItem;
}

- (void)cancelButtonPressed
{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)doneButtonPressed {
    [self dismissViewControllerAnimated:false completion:nil];
    _stopAllTasks = true;
    [assetHolder() executeCallback];
}

- (void)doUnloadView
{
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
    _tableView = nil;
    
    _assetList = nil;
}


- (void) dealloc {
    [KLogger log: [NSString stringWithFormat: @"%s",__PRETTY_FUNCTION__]];
    [_handler reset];
    [actionControlInstance() removeWatcher:self];
    
    [self doUnloadView];
    
    _assetsLibraryDidChangeProxy = nil;
    
    ALAssetsLibrary *assetsLibrary = _assetsLibrary;
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        sharedAssetsLibraryRelease();
                                        [assetsLibrary description];
                                    });
    _assetsLibrary = nil;
}

- (void) addCameraButton {
    //insert first item to asset list
    if (!_assetList) {
        _assetList = [[NSMutableArray alloc] init];
    }
    if (_assetList.count > 0) {
        ImagePickerAsset *asset = [_assetList firstObject];
        if (!asset.cameraAsset) {
            ImagePickerAsset *cameraAsset = [[ImagePickerAsset alloc] init];
            cameraAsset.cameraAsset = true;
            [_assetList insertObject:cameraAsset atIndex:0];
        }
    }
}

- (void) reloadDataOfTableView:(BOOL)scrollToTop {
    //[self addCameraButton];
    [_tableView reloadData];
    if (scrollToTop) {
        _tableView.contentOffset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height);
    }
}

- (void)reloadAssetUrls:(NSArray *)currentAssetList
{
    NSMutableArray *assetList = [[NSMutableArray alloc] initWithArray:currentAssetList];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
                   {
                       CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
                       
                       const int blockSize = 20;
                       
                       NSUInteger count = assetList.count;
                       for (int i = 0; i < count; i++)
                       {
                           if (i % blockSize == 0)
                           {
                               usleep(1 * 1000);
                               
                               if (_stopAllTasks)
                                   return;
                           }
                           ImagePickerAsset *asset = [[assetList objectAtIndex:i] copy];
                           [asset assetUrl];
                       }
                       [KLogger log: [NSString stringWithFormat:@"Parsed assets in %f s", (CFAbsoluteTimeGetCurrent() - startTime) * 1000.0]];
                   });
}

- (void)updateSelectedAssets
{
    if (assetHolder().selectedAssets.count == 0)
        return;
    
//    NSMutableSet *currentUrlsSet = [[NSMutableSet alloc] init];
//    for (ImagePickerAsset *asset in _assetList)
//    {
//        id key = asset.assetUrl;
//        if (key != nil)
//            [currentUrlsSet addObject:key];
//    }
//    
//    [currentUrlsSet intersectSet:[AssetHolder shareInstance].selectedAssets];
//    if (![currentUrlsSet isEqualToSet:[AssetHolder shareInstance].selectedAssets])
//    {
//        [AssetHolder shareInstance].selectedAssets = currentUrlsSet;
//        
//        [self updateSelectionInterface:false];
//    }
}
- (void)updateSelectionInterface:(bool)animated {

}


- (CGFloat)lineHeightForWidth:(CGFloat)width widescreenWidth:(CGFloat)widescreenWidth
{
    return [self lineSpacingForWidth:width widescreenWidth:widescreenWidth] + [self imageSizeForWidth:width widescreenWidth:widescreenWidth];
}

- (int)assetsInRowForWidth:(CGFloat)width widescreenWidth:(CGFloat)widescreenWidth
{
    return (int)(width / [self imageSizeForWidth:width widescreenWidth:widescreenWidth]);
}

- (CGFloat)insetForWidth:(CGFloat)width widescreenWidth:(CGFloat)widescreenWidth
{
    if ([UIScreen mainScreen].scale >= 2.0f - FLT_EPSILON)
    {
        if (widescreenWidth >= 736.0f - FLT_EPSILON)
        {
            if (width >= widescreenWidth - FLT_EPSILON)
                return 2.0f;
            else
                return 0.0f;
        }
        else if (widescreenWidth >= 667.0f - FLT_EPSILON)
        {
            if (width >= widescreenWidth - FLT_EPSILON)
                return 2.0f;
            else
                return 0.0f;
        }
        else
        {
            if (width >= widescreenWidth - FLT_EPSILON)
                return 1.0f;
            else
                return 0.0f;
        }
    }
    else
    {
        if (width >= widescreenWidth - FLT_EPSILON)
            return 1.0f;
        else
            return 0.0f;
    }
}

- (CGFloat)lineSpacingForWidth:(CGFloat)width widescreenWidth:(CGFloat)widescreenWidth
{
    CGFloat imageWidth = [self imageSizeForWidth:width widescreenWidth:widescreenWidth];
    NSInteger numberOfItemPerRow = [self assetsInRowForWidth:width widescreenWidth:widescreenWidth];
    
    return (width - numberOfItemPerRow * imageWidth)/ (numberOfItemPerRow +1);
}

- (CGFloat)imageSizeForWidth:(CGFloat)width widescreenWidth:(CGFloat)widescreenWidth
{
    if ([UIScreen mainScreen].scale >= 2.0f - FLT_EPSILON)
    {
        if (widescreenWidth >= 736.0f - FLT_EPSILON)
        {
            return floor((width - 5) / 6);
        }
        else if (widescreenWidth >= 667.0f - FLT_EPSILON)
        {
            return floor((width - 5) / 5);
        }
    }
    return floor((width - 5) / 4);
}

#pragma mark -

- (void)assetsLibraryDidChange:(NSNotification *)__unused notification
{
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        if (_assetsChangeDelayTimer != nil)
                                        {
                                            [_assetsChangeDelayTimer invalidate];
                                            _assetsChangeDelayTimer = nil;
                                        }
                                        DhtHandle *actionHandle = _handler;
                                        
                                        _assetsChangeDelayTimer = [[DhtRequestTimer alloc] initWithTimeout:1.0 repeat:false completion:^
                                                                   {
                                                                       ImagePickerController *imagePicker = (ImagePickerController *)actionHandle.delegate;
                                                                       if (imagePicker != nil)
                                                                           [imagePicker reloadAssets:false];
                                                                   } queue:assetsProcessingQueue()];
                                        [_assetsChangeDelayTimer start];
                                    });
}


- (void)setAssetsGroup:(ALAssetsGroup *)assetsGroup
{
    _assetsGroup = assetsGroup;
}

- (void)reloadAssets:(bool)firstTime
{
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        [_assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop)
                                         {
                                             if (group != nil)
                                             {
                                                 int groupType = [[group valueForProperty:ALAssetsGroupPropertyType] intValue];
                                                 
                                                 if (_groupUrl == nil)
                                                 {
                                                     if (groupType == ALAssetsGroupSavedPhotos)
                                                     {
                                                         _assetsGroup = group;
                                                         [_assetsGroup setAssetsFilter:[ALAssetsFilter allPhotos]];
                                                     }
                                                     else
                                                     {
                                                         NSURL *currentUrl = [group valueForProperty:ALAssetsGroupPropertyURL];
                                                         NSString *name = [group valueForProperty:ALAssetsGroupPropertyName];
                                                         CGImageRef posterImage = group.posterImage;
                                                         UIImage *icon = posterImage == NULL ? nil : [[UIImage alloc] initWithCGImage:posterImage];
                                                         
                                                         [group setAssetsFilter:[ALAssetsFilter allPhotos]];
                                                         
                                                         NSMutableDictionary *groupDesc = [[NSMutableDictionary alloc] init];
                                                         if (name != nil)
                                                             [groupDesc setObject:name forKey:@"name"];
                                                         if (currentUrl != nil)
                                                             [groupDesc setObject:currentUrl forKey:@"url"];
                                                         if (icon != nil)
                                                             [groupDesc setObject:icon forKey:@"icon"];
                                                         NSInteger count = group.numberOfAssets;
                                                         [groupDesc setObject:[[NSString alloc] initWithFormat:@"(%ld)", (long)count] forKey:@"countString"];
                                                     }
                                                 }
                                                 else
                                                 {
                                                     NSURL *currentUrl = [group valueForProperty:ALAssetsGroupPropertyURL];
                                                     if ([currentUrl isEqual:_groupUrl])
                                                     {
                                                         _assetsGroup = group;
                                                         [_assetsGroup setAssetsFilter:[ALAssetsFilter allPhotos]];
                                                         
                                                         if (stop != NULL)
                                                             *stop = true;
                                                     }
                                                 }
                                             }
                                             else
                                             {
                                                 [self reloadAssetsGroup:firstTime];
                                             }
                                         } failureBlock:^(__unused NSError *error)
                                         {
                                             dispatch_async(dispatch_get_main_queue(), ^
                                                            {
                                                                [self _showAccessDisabled];
                                                            });
                                         }];
                                    });
}

- (void)_showAccessDisabled
{
    
}


- (void)reloadAssetsGroup:(bool)firstTime
{
    CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
    
    dispatchOnAssetsProcessingQueue(^
                                    {
                                        if (_assetsGroup != nil)
                                        {
                                            NSInteger assetCount = [_assetsGroup numberOfAssets];
                                            [KLogger log: [NSString stringWithFormat: @"(%f) assetCount: %ld", (CFAbsoluteTimeGetCurrent() - startTime) * 1000.0, (long)assetCount]];
                                            int enumerateCount = 24;
                                            
                                            NSMutableArray *newAssets = [[NSMutableArray alloc] initWithCapacity:assetCount];
                                            
                                            if (firstTime)
                                            {
                                                for (int i = 0; i < assetCount; i++)
                                                    [newAssets addObject:[[ImagePickerAsset alloc] init]];
                                            }
                                            
                                            [_assetsGroup enumerateAssetsWithOptions:firstTime ? NSEnumerationReverse : 0 usingBlock:^(ALAsset *result, NSUInteger index, __unused BOOL *stop)
                                             {
                                                 if (result != nil && index != NSNotFound)
                                                 {
                                                     ImagePickerAsset *asset = [[ImagePickerAsset alloc] initWithAsset:result];
                                                     
                                                     if (firstTime)
                                                     {
                                                         if (index < assetCount)
                                                             [newAssets replaceObjectAtIndex:index withObject:asset];
                                                         
                                                         if (index < assetCount - enumerateCount - 1)
                                                         {
                                                             if (stop != NULL)
                                                                 *stop = true;
                                                         }
                                                     }
                                                     else
                                                         [newAssets addObject:asset];
                                                 }
                                             }];
                                            
                                            [KLogger log: [NSString stringWithFormat:@"(%f) enumerated %lu assets", (CFAbsoluteTimeGetCurrent() - startTime) * 1000.0, (unsigned long)newAssets.count]];
                                            dispatch_async(dispatch_get_main_queue(), ^
                                                           {
                                                               if (_assetList == nil)
                                                               {
                                                                   _tableView.alpha = 0.0f;
                                                                   [UIView animateWithDuration:0.25 animations:^
                                                                    {
                                                                        _tableView.alpha = 1.0f;
                                                                    }];
                                                               }
                                                               
                                                               _tableView.scrollEnabled = !firstTime;
                                                               _assetList = newAssets;
                                                               [self reloadDataOfTableView:firstTime];
                                                               
                                                               if (firstTime)
                                                               {
                                                                   UIEdgeInsets inset = _tableView.contentInset;
                                                                   CGPoint contentOffset = CGPointMake(0, _tableView.contentSize.height - _tableView.frame.size.height + inset.bottom);
                                                                   if (contentOffset.y < -inset.top)
                                                                       contentOffset.y = -inset.top;
                                                                   [_tableView setContentOffset:contentOffset animated:false];
                                                               }
                                                               
                                                               if (assetHolder().selectedAssets.count != 0)
                                                                   [self updateSelectedAssets];
                                                           });
                                            
                                            if (firstTime)
                                            {
                                                [self reloadAssetsGroup:false];
                                            }
                                            else
                                                [self reloadAssetUrls:newAssets];
                                        }
                                        else
                                        {
                                        }
                                    });
}

#pragma mark -

- (NSInteger)tableView:(UITableView *)__unused tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return _assetList.count / _assetsInRow + (_assetList.count % _assetsInRow != 0 ? 1 : 0);
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)__unused tableView heightForRowAtIndexPath:(NSIndexPath *)__unused indexPath
{
    if (indexPath.section == 0)
        return _lineHeight;
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        NSInteger rowStartIndex = indexPath.row * _assetsInRow;
        if (rowStartIndex < _assetList.count)
        {
            static NSString *imageCellIdentifier = @"IC";
            ImagePickerCell *imageCell = (ImagePickerCell *)[tableView dequeueReusableCellWithIdentifier:imageCellIdentifier];
            if (imageCell == nil)
            {
                imageCell = [[ImagePickerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:imageCellIdentifier selectionControls:true imageSize: IsRetina() ? 78.5f : 78.0f];
                imageCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            [imageCell resetImages:_assetsInRow imageSize:_imageSize inset:_inset];
            
            NSInteger assetListCount = _assetList.count;
            for (NSInteger i = rowStartIndex; i < rowStartIndex + _assetsInRow && i < assetListCount; i++)
            {
                ImagePickerAsset *asset = [_assetList objectAtIndex:i];
                UIImage *image = [asset forceLoadedThumbnailImage];
                bool isSelected = false;
                if (assetHolder().selectedAssets.count != 0)
                {
                    id key = asset.assetUrl;
                    isSelected = key == nil ? false : [assetHolder() assetIsExistedWithUrl:key];
                }
                [imageCell addAsset:asset isSelected:isSelected withImage:image];
            }
            
            if (_hideItemUrl != nil)
                [imageCell hideImage:_hideItemUrl hide:true];
            
            return imageCell;
        }
    }
    
    UITableViewCell *emptyCell = [tableView dequeueReusableCellWithIdentifier:@"EmptyCell"];
    if (emptyCell == nil)
    {
        emptyCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EmptyCell"];
        emptyCell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return emptyCell;
}

- (void)assetTapped:(ImagePickerAsset *)asset imageCell:(ImagePickerCell *)imageCell
{
    [self assetSelected:asset imageCell:imageCell];
}


#pragma --mark Image Picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    if ([info objectForKey:@"UIImagePickerControllerOriginalImage"]) {
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        __weak typeof(self) weakself = self;
        
        [[ImagePickerController sharedAssetsLibrary] writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error) {
            if (error == nil) {
                __strong typeof(self) strongself = weakself;
                [ImagePickerController loadAssetWithUrl:assetURL completion:^(ALAsset *asset) {
                    ImagePickerAsset *pkasset = [[ImagePickerAsset alloc] initWithAsset:asset];
                    [strongself assetSelected:pkasset imageCell:nil];
                    [Utils dispatchOnMainAsyncSafe:^{
                        [strongself doneButtonPressed];
                    }];
                }];
            }
        }];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:true completion:nil];
}

- (void) openCamera {
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera ]) {
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:pickerController animated:true completion:nil];
    }
}

- (void)deselectAll {
    for (ImagePickerCell *cell in self.tableView.visibleCells) {
        [cell deSelectAll];
    }
}

- (void)assetSelected:(ImagePickerAsset *)asset imageCell:(ImagePickerCell *)imageCell {
    if (asset.cameraAsset) {
        if (![assetHolder() isFull]) {
            [self openCamera];
        }
    } else {
        if (asset.assetUrl == nil )
            return;
        [assetHolder() clear];
        [self deselectAll];
        
        bool isSelected = false;
        if (![assetHolder() isFull]) {
            isSelected = true;
            PostNewObject *obj = [[PostNewObject alloc] initWithAsset:asset];
            [assetHolder().selectedAssets addObject:obj];
            [imageCell animateImageSelected:asset.assetUrl isSelected:isSelected];
        }
        
        [self updateSelectionInterface:true];
    }
    
}
@end
