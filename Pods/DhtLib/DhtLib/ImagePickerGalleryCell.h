//
//  ImagePickerGalleryCell.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickerGalleryCell : UITableViewCell
- (void)setIcon:(UIImage *)icon icon2:(UIImage *)icon2 icon3:(UIImage *)icon3;
- (void)setTitle:(NSString *)title countString:(NSString *)countString;
- (void)setTitleAccentColor:(bool)accent;
@end
