//
//  SelectionView.h
//  kids_taxi
//
//  Created by Nguyen Van Dung on 3/15/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectionView : UIView
@property (nonatomic, strong) UIImageView   *shadowView;
@property (nonatomic, strong) UIImageView   *checkMarView;
@end
