//
//  Utils.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/5/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <pthread.h>
#include <sys/sysctl.h>
#import <CoreText/CoreText.h>
#define __SYNCHRONIZED_DEFINE(lock) pthread_mutex_t ____SYNCHRONIZED_##lock
#define __SYNCHRONIZED_INIT(lock) pthread_mutex_init(&____SYNCHRONIZED_##lock, NULL)
#define __SYNCHRONIZED_BEGIN(lock) pthread_mutex_lock(&____SYNCHRONIZED_##lock);
#define __SYNCHRONIZED_END(lock) pthread_mutex_unlock(&____SYNCHRONIZED_##lock);
#define __SYNCHRONIZED_INIT(lock) pthread_mutex_init(&____SYNCHRONIZED_##lock, NULL)


@interface DhtUtility : NSObject
{
    
}


@end
