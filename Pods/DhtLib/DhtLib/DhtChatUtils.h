//
//  DhtChatUtils.h
//  DhtChat
//
//  Created by Nguyen Van Dung on 10/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifdef __cplusplus
extern "C" {
#endif
    CGFloat DhtScreenScaling();
    UIColor* colorRGB(int rgb, float alpha);
#ifdef __cplusplus
}
#endif

@interface DhtChatUtils : NSObject
+ (NSArray *)textCheckingResults:(NSString *)_text;
+ (NSArray *)additionAttributeForUrls:(NSArray *)checkingResult;
+ (CGContextRef)createContentContext:(CGSize)size;
+ (NSString *)generateUUID;
+ (NSString *)tempPath;
+ (NSString *)recordedPath;
+ (NSString *)documentPath;
+ (CGSize) sizeForText:(NSString *)text
             boundSize:(CGSize)bsize
                option:(NSStringDrawingOptions)option
         lineBreakMode:(NSLineBreakMode)lineBreakMode
                  font:(UIFont *)font;
@end
