//
//  DhtAudioPlayer.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 2/4/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@class  DhtAudioPlayer;
@protocol DhtAudioPlayerDelegate <NSObject>
@optional
- (void)audioPlayerDidFinishPlaying:(DhtAudioPlayer *)audioPlayer;

@end

@protocol DhtInlineAudioPlayerDelegate <NSObject>
- (void)mediaPlaybackStateUpdated:(bool)isPaused playbackPosition:(float)playbackPosition timestamp:(NSTimeInterval)timestamp preciseDuration:(NSTimeInterval)preciseDuration;
@end

@interface DhtAudioPlayer : NSObject
@property (nonatomic, copy) NSString    *identifier;
@property (nonatomic, assign) BOOL  isDefault;
@property (nonatomic, weak) id<DhtAudioPlayerDelegate> delegate;
@property (nonatomic, weak) id <DhtInlineAudioPlayerDelegate> inLineDelegate;

- (void)removeInlineDelegateObject:(id<DhtInlineAudioPlayerDelegate>)object;
- (instancetype)initWithPath:(NSString *) path;
- (instancetype)initWithData:(NSData *)data;
- (BOOL)isReady;
- (void)play;
- (void)playFromPosition:(NSTimeInterval)position;
- (void)pause;
- (void)stop;
- (NSTimeInterval)currentPositionSync:(bool)sync;
- (NSTimeInterval)duration;
- (NSTimeInterval)currentTime;
- (float)playbackPosition:(NSTimeInterval *)timestamp sync:(bool)sync;
- (BOOL) isPaused;
@end
