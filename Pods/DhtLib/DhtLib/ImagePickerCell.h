//
//  ImagePickerCell.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagePickerAsset.h"
#import "ImageInfo.h"

@class ImagePickerCell;

@protocol ImagePickerCellDelegate <NSObject>
- (void)imagePickerCell:(ImagePickerCell *)cell selectedSearchId:(int)searchId imageInfo:(ImageInfo *)imageInfo;
- (void)imagePickerCell:(ImagePickerCell *)cell tappedSearchId:(int)searchId imageInfo:(ImageInfo *)imageInfo thumbnailImage:(UIImage *)thumbnailImage;

@end


@interface ImagePickerCell : UITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier selectionControls:(bool)selectionControls imageSize:(float)imageSize;
- (void)resetImages:(int)imagesInRow imageSize:(CGFloat)imageSize inset:(CGFloat)inset;
- (void)addAsset:(ImagePickerAsset *)asset isSelected:(bool)isSelected withImage:(UIImage *)image;
- (void)addImage:(ImageInfo *)imageInfo searchId:(int)searchId isSelected:(bool)isSelected;
- (void)animateImageSelected:(id)itemId isSelected:(bool)isSelected;
- (void)updateImageSelected:(id)itemId isSelected:(bool)isSelected;
- (NSString *)assetUrlAtPoint:(CGPoint)point;
- (CGRect)rectForAsset:(NSString *)assetUrl;
- (CGRect)rectForSearchId:(int)searchId;
- (UIView *)hideImage:(id)itemId hide:(bool)hide;
- (UIImage *)imageForSearchId:(int)searchId;
- (NSString *)currentImageUrlForSearchId:(int)searchId;
- (void)deSelectAll;

@end
