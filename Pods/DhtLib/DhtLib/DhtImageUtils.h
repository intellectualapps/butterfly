//
//  DhtImageUtils.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/5/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DhtImageUtils : NSObject
+ (UIImage *)d_ScaleImage:(UIImage *)image size:(CGSize)size roundCorner:(int)radius opaque:(BOOL)opaque overlayImage:(UIImage *)overlay;
+ (UIImage *)d_ImageWith:(CGFloat)radius color:(UIColor *)color;
+ (UIImage *)d_BlurImage:(UIImage *)image blurData:(__autoreleasing NSData **)blurredData;
@end
