//
//  DhtNotificationWindow.h
//  Fonext
//
//  Created by nguyen van dung on 4/23/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationProtocol.h"
@class ActionHandle;

@interface DhtNotificationWindow : UIWindow
{
    
}

@property (nonatomic) float windowHeight;
@property (nonatomic, readonly) bool isDismissed;
@property (nonatomic, strong) ActionHandle *watcher;
@property (nonatomic, strong) NSString *watcherAction;
@property (nonatomic, strong) NSDictionary *watcherOptions;
- (void)animateIn;
- (void)animateOut;
- (void)setContentView:(UIView *)view;
- (UIView *)contentView;
- (void)performTapAction:(id<NotificationProtocol>)object;
- (void)openViewControllerWithObject:(id<NotificationProtocol>)object;
@end
