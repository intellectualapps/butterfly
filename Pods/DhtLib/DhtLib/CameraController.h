//
//  CameraController.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/6/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CameraControllerDelegate <NSObject>

@optional

- (void)cameraControllerCapturedVideoWithTempFilePath:(NSString *)tempVideoFilePath fileSize:(int32_t)fileSize previewImage:(UIImage *)previewImage duration:(NSTimeInterval)duration dimensions:(CGSize)dimenstions assetUrl:(NSString *)assetUrl;
- (void)cameraControllerCompletedWithExistingMedia:(id)media;
- (void)cameraControllerCompletedWithNoResult;
- (void)cameraControllerCompletedWithDocument:(NSURL *)fileUrl fileName:(NSString *)fileName mimeType:(NSString *)mimeType;

@end

@interface CameraController : UIImagePickerController

@property (nonatomic, weak) id<CameraControllerDelegate> completionDelegate;
@property (nonatomic) bool storeCapturedAssets;
@property (nonatomic) bool isInDocumentMode;
@property (nonatomic) bool avatarMode;
@end
